// import { Component, Input, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
// import { ModalController } from '@ionic/angular';
// import { TranslateService } from '@ngx-translate/core';
// import { FrequencyDto } from 'src/app/share/Dto/FrequencyDto';
// import { Survey } from 'src/app/share/Dto/SurveyDto';
// import { SurveyVersion } from 'src/app/share/Dto/SurveyVersionDto';
// import { UserDto } from 'src/app/share/Dto/UserDto';
// import { UserSurvey } from 'src/app/share/Dto/UserSurveyDto';
// import { FrequencyRestService } from 'src/app/share/frequency-rest.service';
// import { SurveyRestService } from 'src/app/share/survey-rest.service';
// import { UserRestService } from 'src/app/share/user-rest.service';
// import { UserSurveyRestService } from 'src/app/share/user-survey-rest.service';
// // import { SurveyGroupItem } from '../home.page';

// @Component({
//   selector: 'app-survey-group',
//   templateUrl: './survey-group.component.html',
//   styleUrls: ['./survey-group.component.scss'],
// })
// export class SurveyGroupComponent implements OnInit {

//   // @Input() groupItems: SurveyGroupItem[];

//   currentSurveyVersion: SurveyVersion;
//   aYearLater: string = "";

//   constructor(
//     public modalCtrl: ModalController,
//     private router: Router,
//     private userRestService: UserRestService,
//     private userSurveyRestService: UserSurveyRestService,
//     private surveyRestService: SurveyRestService,
//     private frequenceRestService: FrequencyRestService,
//     private translateService: TranslateService
//   ) { }

//   ngOnInit() {
//     this.aYearLater = ""
//     this.initHexagon();
//   }

//   async getUser() {
//     return await this.userRestService.getMe().toPromise();
//   }

//   async getAllUserSurvey(user: UserDto, surveyShortName: string) {
//     if (new Date() < new Date(user.creationDate)) {
//       this.router.navigate(['errorPage'])
//     } else {
//       return this.userSurveyRestService.getAllUserSurveyByUserIdAndSurveyName(surveyShortName, user.id).toPromise();
//     }
//   }

//   async getSurvey() {
//     return await this.surveyRestService.getAll().toPromise();
//   }

//   async getSurveyVersion(surveyId: string) {
//     return await this.surveyRestService.getLatestSurveyVersion(surveyId).toPromise();
//   }

//   async getFrequencies() {
//     return this.frequenceRestService.getAllFrequences().toPromise();
//   }

//   async checkFrequency(allUserSurvey: UserSurvey[], surveyShortName: string): Promise<boolean> {
//     return new Promise(async (resolve, reject) => {
//       //Get all frequencies
//       const allFrequencies: FrequencyDto[] = await this.getFrequencies();
//       const yearFrequency: FrequencyDto = allFrequencies.find(f => f.annualized_value == 365);
//       const monthFrequency: FrequencyDto = allFrequencies.find(f => f.annualized_value == 30);
//       const weekFrequency: FrequencyDto = allFrequencies.find(f => f.annualized_value == 7);
//       const uniqueFrequency: FrequencyDto = allFrequencies.find(f => f.annualized_value == 0);

//       //Find current surveyVersion
//       const surveyList: Survey[] = await this.getSurvey();
//       const survey = surveyList.find(survey => survey.shortName == surveyShortName);
//       const surveyVersion: SurveyVersion = await this.getSurveyVersion(survey.id);
//       this.currentSurveyVersion = surveyVersion;


//       if (this.currentSurveyVersion.frequency.id == yearFrequency.id) {
//         await this.handleYearFrequency(allUserSurvey, surveyShortName);
//       }
//       if (this.currentSurveyVersion.frequency.id == monthFrequency.id) {
//         await this.handleMonthFrequency(allUserSurvey, surveyShortName);
//       }
//       if (this.currentSurveyVersion.frequency.id == weekFrequency.id) {
//         await this.handleWeekFrequency(allUserSurvey, surveyShortName);
//       }
//       if (this.currentSurveyVersion.frequency.id == uniqueFrequency.id) {
//         await this.handleUniqueFrequency(allUserSurvey, surveyShortName);
//       }
//     });
//   }

//   async handleYearFrequency(allUserSurvey: UserSurvey[], surveyShortName: string) {
//     var today = new Date();
//     //Sort allusersurvey by date (recent to older)
//     if (allUserSurvey == null || allUserSurvey.length == 0) {
//       await this.createNewUserSurvey(today, surveyShortName);
//     } else {
//       allUserSurvey.sort(function (a, b) {
//         return b.startDate - a.startDate;
//       });
//     }

//     //get the latest one
//     var latestUserSurvey = allUserSurvey[0];

//     if (latestUserSurvey.dateFilled == null) {
//       await this.handleUserSurvey(latestUserSurvey, today, surveyShortName, "year")
//     } else {
//       var aYearLater = new Date(latestUserSurvey.dateFilled);
//       aYearLater.setFullYear(aYearLater.getFullYear() + 1);
//       this.aYearLater = new Date(aYearLater.setHours(0, 0, 0, 0)).toLocaleDateString('en-US');

//       // //add a year + 7 day (that create delay 
//       // var aYearLaterDelay = new Date(aYearLater);
//       // aYearLaterDelay.setDate(aYearLaterDelay.getDate() + 7)

//       if (today >= aYearLater) {
//         var currenUserSurvey = allUserSurvey.find(us => new Date(us.startDate).getFullYear() == aYearLater.getFullYear())
//         if (currenUserSurvey != null) {
//           await this.handleUserSurvey(currenUserSurvey, today, surveyShortName, "year")
//         } else {
//           await this.createNewUserSurvey(today, surveyShortName);
//         }
//       } else {
//         this.translateService.get('SurveyGroup.FrequencyWarningSurveyAlreadyCompletedYear', { date: this.aYearLater }).subscribe((res) => {
//           alert(res);
//         })
//       }
//     }



//     if (latestUserSurvey != null) {
//     } else {
//       await this.createNewUserSurvey(today, surveyShortName);
//     }
//   }

//   async handleMonthFrequency(allUserSurvey: UserSurvey[], surveyShortName: string) {
//     var today = new Date();
//     var currentMonth = new Date().getMonth();
//     var lastDayOfDelay = new Date(today.getFullYear(), today.getMonth(), 7);
//     var firstDayOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);

//     var currenUserSurvey = allUserSurvey.find(us => new Date(us.startDate).getMonth() == currentMonth)

//     if (today <= lastDayOfDelay) {
//       if (currenUserSurvey != null) {
//         await this.handleUserSurvey(currenUserSurvey, firstDayOfMonth, surveyShortName, "month")
//       }
//       if (currenUserSurvey == null) {
//         await this.createNewUserSurvey(firstDayOfMonth, surveyShortName);
//       }
//     } else {
//       await this.throwFrequencyWarningOutOfDelay("month");
//     }
//   }

//   async handleWeekFrequency(allUserSurvey: UserSurvey[], surveyShortName: string) {
//     // TODO: Implementer week frequency -> demander les regles
//   }

//   async handleUniqueFrequency(allUserSurvey: UserSurvey[], surveyShortName: string) {
//     var today = new Date();

//     if (allUserSurvey.length != 0) {
//       if (allUserSurvey.length >= 1) {
//         this.handleUserSurvey(allUserSurvey[0], today, surveyShortName, "unique");
//       }
//     } else {
//       this.createNewUserSurvey(today, surveyShortName);
//     }
//   }

//   async handleUserSurvey(currentUserSurvey: UserSurvey, choosedDate: Date, surveyShortName: string, frequency: string) {
//     if (currentUserSurvey != null && currentUserSurvey.dateFilled == null) {
//       this.router.navigate(['survey', { surveyShortName: surveyShortName, choosedDate: currentUserSurvey.startDate, userSurveyId: currentUserSurvey.id }]).then(() => {
//         this.closeSurveyGroupModal();
//       });;
//     }
//     if (currentUserSurvey != null && currentUserSurvey.dateFilled != null) {
//       await this.throwFrequencyWarningSurveyAlreadyCompleted(frequency)
//     }
//   }

//   async createNewUserSurvey(choosedDate: Date, surveyShortName: String) {
//     this.router.navigate(['survey', { choosedDate: choosedDate, surveyShortName: surveyShortName }]).then(() => {
//       this.closeSurveyGroupModal();
//     });
//   }



//   async throwFrequencyWarningSurveyAlreadyCompleted(frequency: string) {
//     if (frequency.toLocaleLowerCase() == "year") {
//       this.translateService.get('SurveyGroup.FrequencyWarningSurveyAlreadyCompletedYear', { date: this.aYearLater }).subscribe((res) => {
//         alert(res);
//       })
//     }
//     if (frequency.toLocaleLowerCase() == "month") {
//       this.translateService.get('SurveyGroup.FrequencyWarningSurveyAlreadyCompletedMonth').subscribe((res) => {
//         alert(res);
//       })
//     }
//     if (frequency.toLocaleLowerCase() == "week") {
//       this.translateService.get('MedISurveyGroupnformation.FrequencyWarningSurveyAlreadyCompletedWeek').subscribe((res) => {
//         alert(res);
//       })
//     }
//     if (frequency.toLocaleLowerCase() == "unique") {
//       this.translateService.get('SurveyGroup.FrequencyWarningSurveyAlreadyCompletedUnique').subscribe((res) => {
//         alert(res);
//       })
//     }
//   }

//   async throwFrequencyWarningOutOfDelay(frequency: string) {
//     if (frequency.toLocaleLowerCase() == "year") {
//       this.translateService.get('SurveyGroup.FrequencyWarningOutOfDelayYear').subscribe((res) => {
//         alert(res);
//       })
//     }
//     if (frequency.toLocaleLowerCase() == "month") {
//       this.translateService.get('SurveyGroup.FrequencyWarningOutOfDelayMonth').subscribe((res) => {
//         alert(res);
//       })
//     }
//     if (frequency.toLocaleLowerCase() == "week") {
//       this.translateService.get('SurveyGroup.FrequencyWarningOutOfDelayWeek').subscribe((res) => {
//         alert(res);
//       })
//     }
//     if (frequency.toLocaleLowerCase() == "unique") {
//       // TODO: Implementer un message en cas de sondage unique oublié 
//       this.translateService.get('SurveyGroup.FrequencyWarningSurveyAlreadyCompletedUnique').subscribe((res) => {
//         alert(res);
//       })
//     }
//   }


//   initHexagon() {
//     const component = this;
//     var customIndexList = [2, 5, 7, 6, 3, 1]

//     this.groupItems.forEach(function (item, index) {
//       const customIndex = customIndexList[index];
//       setTimeout(() => {
//         document.getElementById('hex-text-' + customIndex).innerHTML = item.text;

//         var hex = document.getElementById('hex-' + customIndex);
//         hex.classList.remove('hidden');

//         hex.onclick = async function () {
//           const user = await component.getUser();
//           const allUserSurvey = await component.getAllUserSurvey(user, item.destinationPath);
//           await component.checkFrequency(allUserSurvey, item.destinationPath);
//         }
//       }, 125 * (index + 1));
//     });
//   }

//   closeSurveyGroupModal() {
//     this.modalCtrl.dismiss();
//   }
// }