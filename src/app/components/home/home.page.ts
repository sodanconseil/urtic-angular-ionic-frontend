// import { Component, NgModule, OnInit, ViewChild } from '@angular/core';
// import { Router } from '@angular/router';
// import { AuthService } from '../../keycloak/auth.service';
// import { CompletProfileFormComponent } from '../complete-profile-form/complete-profile-form.component';
// import { ModalController } from '@ionic/angular';
// import { UserRestService } from 'src/app/share/user-rest.service';
// import { SurveyGroupComponent } from './survey-group/survey-group.component';

// //Translation
// import { TranslateService } from '@ngx-translate/core';
// //

// @Component({
//   selector: 'app-home',
//   templateUrl: 'home.page.html',
//   styleUrls: ['home.page.scss']
// })

// export class HomePage implements OnInit {
//   // language: string = this.translateService.currentLang;

//   public token;
//   isLoggedIn = JSON.stringify(window.localStorage.getItem('isLoggedIn')) == null ? JSON.stringify(window.localStorage.getItem('isLoggedIn')) : false
//   currentUser: any = null;

//   enableBackdropDismiss = false;
//   showBackdrop = false;
//   shouldPropagate = false;

//   // urticLogo = "../../../assets/icons/urtic_logo.png"
//   urticLogo = "../../../assets/icons/logo_animation.gif"
//   loadingSplashScreen = true;

//   constructor(private authService: AuthService,
//     private router: Router,
//     public modalCtrl: ModalController,
//     private userRestService: UserRestService,
//     private translateService: TranslateService,
//   ) {
//   }

//   async ngOnInit() {
//     let component = this;
//     await setTimeout(async function () {
//       component.loadingSplashScreen = false;
//       await component.login().then(() => {
//         component.isLoggedIn = true;
//       })
//     }, 3000);
//   }

//   //KEYCLOAK FUNCTIONS
//   async login() {
//     if (this.isLoggedIn == false) {
//       await this.authService.keycloakLogin(true).then((value) => {
//         this.token = value;
//         if (value) {
//           window.localStorage.setItem('isLoggedIn', JSON.stringify(true));
//           this.isLoggedIn = JSON.parse(window.localStorage.getItem('isLoggedIn'));
//           window.localStorage.setItem('OAUTH_TOKENS', JSON.stringify(this.token));
//           Promise.resolve()
//         }
//       });

//       this.getUser();

//     }
//   }

//   getUser() {
//     this.userRestService.getKeycloakUser()
//       .subscribe((res) => {
//         if (res != null) {
//           this.currentUser = res;
//           window.localStorage.setItem('currentUser', JSON.stringify(res));
//           this.translateService.setDefaultLang(res.prefferedLanguage)
//           this.translateService.currentLang = res.prefferedLanguage;
//           this.translateService.use(res.prefferedLanguage)
//         }
//       }, (err) => {
//         if (err != null) {
//           this.showCompleteProfile();
//         }
//       })
//   }

//   async logout() {
//     this.authService.keycloakLogout(true)
//     return Promise.resolve().then(() => {
//       window.localStorage.clear();
//       window.localStorage.setItem('isLoggedIn', JSON.stringify(false));
//       window.localStorage.setItem('completeProfileCompleted', JSON.stringify(false));
//       this.isLoggedIn = false;
//       this.currentUser = null;
//       window.location.reload();
//     });
//   }

//   async showCompleteProfile() {
//     const modal = await this.modalCtrl.create({
//       component: CompletProfileFormComponent,
//     });
//     return await modal.present();
//   }

//   async showGroupOfSurvey(items: SurveyGroupItem[]) {
//     const modal = await this.modalCtrl.create({
//       component: SurveyGroupComponent,
//       animated: false,
//       showBackdrop: false,
//       componentProps: {
//         groupItems: items,
//       }
//     });
//     return await modal.present();
//   }


//   goToMedManager() {
//     this.router.navigate(['medManager']);
//   }

//   gotToUas7Survey() {
//     this.router.navigate(['surveyCalendar', "UAS7"]);
//   }

//   gotToAas7Survey() {
//     this.router.navigate(['surveyCalendar', "AAS7"]);
//   }

//   gotToUctSurvey() {
//     var UrticariaText = "";
//     var AngioedemaText = "";
//     this.translateService.get('Homepage.Urticaria').subscribe((res: string) => {
//       UrticariaText = res;
//     })
//     this.translateService.get('Homepage.Angioedema').subscribe((res: string) => {
//       AngioedemaText = res;
//     })
//     const surveyGroup: SurveyGroupItem[] = [
//       { text: UrticariaText, destinationPath: 'UCT' },
//       { text: AngioedemaText, destinationPath: 'ACT' },
//     ];
//     this.showGroupOfSurvey(surveyGroup);
//   }

//   gotToQolSurvey() {
//     var CUQOLtext = "";
//     var AEQOLtext = "";
//     this.translateService.get('Homepage.CUQOL').subscribe((res: string) => {
//       CUQOLtext = res;
//     })
//     this.translateService.get('Homepage.AEQOL').subscribe((res) => {
//       AEQOLtext = res;
//     })
//     const surveyGroup: SurveyGroupItem[] = [
//       { text: CUQOLtext, destinationPath: 'CUQOL' },
//       { text: AEQOLtext, destinationPath: 'AEQOL' },
//     ];
//     this.showGroupOfSurvey(surveyGroup);
//   }

//   gotToMedHistory() {
//     var historyMedical = "";
//     this.translateService.get('Homepage.MedicalHistory').subscribe((res) => {
//       historyMedical = res;
//     });

//     const surveyGroup: SurveyGroupItem[] = [
//       { text: historyMedical, destinationPath: 'HM' },
//     ];
//     this.showGroupOfSurvey(surveyGroup);
//   }

//   goToProfile() {
//     this.router.navigate(['profile']);
//   }

//   goToPicture() {
//     this.router.navigate(['picture']);
//   }

//   // //Translation
//   // languageChange() {
//   //   if (this.language == "en") {
//   //     this.language = "fr";
//   //   } else {
//   //     this.language = "en";
//   //   }
//   //   this.translateService.use(this.language);
//   // }
//   // //
// }



// export interface SurveyGroupItem {
//   text: string;
//   destinationPath: string;
// }



//######################################################
//##################### NEW UI #########################
//######################################################

import { Component, NgModule, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../keycloak/auth.service';
import { CompletProfileFormComponent } from '../complete-profile-form/complete-profile-form.component';
import { ModalController } from '@ionic/angular';
import { UserRestService } from 'src/app/share/user-rest.service';
// import { SurveyGroupComponent } from './survey-group/survey-group.component';

//Translation
import { TranslateService } from '@ngx-translate/core';
import { FrequencyRestService } from 'src/app/share/frequency-rest.service';
import { FrequencyDto } from 'src/app/share/Dto/FrequencyDto';
import { SurveyRestService } from 'src/app/share/survey-rest.service';
import { Survey } from 'src/app/share/Dto/SurveyDto';
import { SurveyVersion } from 'src/app/share/Dto/SurveyVersionDto';
import { UserDto } from 'src/app/share/Dto/UserDto';
import { UserSurvey } from 'src/app/share/Dto/UserSurveyDto';
import { UserSurveyRestService } from 'src/app/share/user-survey-rest.service';
//

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})

export class HomePage implements OnInit {
  // language: string = this.translateService.currentLang;

  public token;
  isLoggedIn = JSON.stringify(window.localStorage.getItem('isLoggedIn')) == null ? JSON.stringify(window.localStorage.getItem('isLoggedIn')) : false
  currentUser: any = null;

  enableBackdropDismiss = false;
  showBackdrop = false;
  shouldPropagate = false;

  profileImage = "../../../assets/icons/profile.png";
  medImage = "../../../assets/icons/pills.png";
  commnentImage = "../../../assets/icons/comment.png";
  pictureImage = "../../../assets/icons/image.png";
  logoutImage = "../../../assets/icons/logout.png";

  // urticLogo = "../../../assets/icons/urtic_logo.png"
  urticLogo = "../../../assets/icons/logo_animation.gif"
  loadingSplashScreen = true;

  allFrequency: FrequencyDto[] = [];
  allSurvey: Survey[] = [];
  allSurveyVersion: SurveyVersion[] = [];

  homePageListItems: homePageListItem[] = [];

  dailySurvey: checkboxAnswer[] = [];
  weeklySurvey: checkboxAnswer[] = [];
  monthlysurvey: checkboxAnswer[] = [];
  yearllySurvey: checkboxAnswer[] = [];
  uniqueSurvey: checkboxAnswer[] = [];

  constructor(
    private authService: AuthService,
    private router: Router,
    public modalCtrl: ModalController,
    private userRestService: UserRestService,
    private translateService: TranslateService,
    private frequencyRestService: FrequencyRestService,
    private surveyRestService: SurveyRestService,
    private userSurveyRestService: UserSurveyRestService
  ) {
  }

  async ngOnInit() {
    let component = this;
    await setTimeout(async function () {
      component.loadingSplashScreen = false;
      await component.login().then(() => {
        component.isLoggedIn = true;
      })
    }, 3000);
  }

  //KEYCLOAK FUNCTIONS
  async login() {
    if (this.isLoggedIn == false) {
      await this.authService.keycloakLogin(true).then((value) => {
        this.token = value;
        if (value) {
          window.localStorage.setItem('isLoggedIn', JSON.stringify(true));
          this.isLoggedIn = JSON.parse(window.localStorage.getItem('isLoggedIn'));
          window.localStorage.setItem('OAUTH_TOKENS', JSON.stringify(this.token));
          Promise.resolve()
        }
      });

      this.getKeycloakUser();

    }
  }

  getKeycloakUser() {
    this.userRestService.getKeycloakUser()
      .subscribe((res) => {
        if (res != null) {
          this.currentUser = res;
          window.localStorage.setItem('currentUser', JSON.stringify(res));
          this.translateService.setDefaultLang(res.prefferedLanguage)
          this.translateService.currentLang = res.prefferedLanguage;
          this.translateService.use(res.prefferedLanguage)
          this.getItems()
        }
      }, (err) => {
        if (err != null) {
          this.showCompleteProfile().then(() => {
            this.getItems();
          });
        }
      })
  }

  async logout() {
    this.authService.keycloakLogout(true)
    return Promise.resolve().then(() => {
      window.localStorage.clear();
      window.localStorage.setItem('isLoggedIn', JSON.stringify(false));
      window.localStorage.setItem('completeProfileCompleted', JSON.stringify(false));
      this.isLoggedIn = false;
      this.currentUser = null;
      window.location.reload();
    });
  }

  async showCompleteProfile() {
    const modal = await this.modalCtrl.create({
      component: CompletProfileFormComponent,
    });
    return await modal.present();
  }

  async getItems() {
    this.getAllFrequency();
    this.getAllSurvey();
  }

  async ionViewWillEnter() {
    this.homePageListItems = [];
    this.dailySurvey = [];
    this.weeklySurvey = [];
    this.monthlysurvey = [];
    this.yearllySurvey = [];
    this.uniqueSurvey = [];
    this.allFrequency = [];
    this.allSurvey = [];

    this.getKeycloakUser();
  }

  getAllFrequency() {
    this.frequencyRestService.getAllFrequences().subscribe((res: FrequencyDto[]) => {
      if (res) {

        for (let index = 0; index < res.length; index++) {
          const frequency = res[index];

          //Translation
          if (this.translateService.currentLang == "en") {
            frequency.name = frequency.en;
          } if (this.translateService.currentLang == "fr") {
            frequency.name = frequency.fr;
          }
          //

          //Unique
          if (frequency.annualized_value == 0) {
            this.homePageListItems.push({
              surveyList: this.uniqueSurvey,
              frequency: frequency
            })
          }

          //Daily
          if (frequency.annualized_value == 1) {
            this.homePageListItems.push({
              surveyList: this.dailySurvey,
              frequency: frequency
            })
          }

          //Weekly
          if (frequency.annualized_value == 7) {
            this.homePageListItems.push({
              surveyList: this.weeklySurvey,
              frequency: frequency
            })
          }

          //Monthly
          if (frequency.annualized_value == 30) {
            this.homePageListItems.push({
              surveyList: this.monthlysurvey,
              frequency: frequency
            })
          }

          //Yearly
          if (frequency.annualized_value == 365) {
            this.homePageListItems.push({
              surveyList: this.yearllySurvey,
              frequency: frequency
            })
          }
        }
        this.allFrequency = res
        this.allFrequency.sort((frequency1, frequency2) => frequency1.annualized_value - frequency2.annualized_value);
      }
    });
  }

  async getAllSurvey() {
    this.surveyRestService.getAll().subscribe((res: Survey[]) => {
      if (res) {
        this.allSurvey = res;

        res.forEach(async survey => {
          var latestSurvey: SurveyVersion = await this.getLatestSurvey(survey);
          if (latestSurvey != null) {
            await this.fillFrequencySurveyList(latestSurvey);
          }
        });
      }
    });
  }


  async getAllUserSurvey(user: UserDto, surveyShortName: string) {
    return this.userSurveyRestService.getAllUserSurveyByUserIdAndSurveyName(surveyShortName, user.id).toPromise();
  }

  async fillFrequencySurveyList(latestSurvey: SurveyVersion) {
    var currentHomeListItem = this.homePageListItems.find(hli => hli.frequency.annualized_value == latestSurvey.frequency.annualized_value);

    var allUserSurvey: UserSurvey[] = await this.getAllUserSurvey(this.currentUser, latestSurvey.shortName)

    var newCheckboxAnswer: checkboxAnswer = null

    if (allUserSurvey != null) {
      newCheckboxAnswer = await this.handleAllUserSurvey(latestSurvey.frequency, allUserSurvey);
      newCheckboxAnswer.surveyVersion = latestSurvey;
      debugger;
    }

    //translation
    if (this.translateService.currentLang == "en") {
      latestSurvey.name = latestSurvey.en;
    } if (this.translateService.currentLang == "fr") {
      latestSurvey.name = latestSurvey.fr;
    }
    //


    currentHomeListItem.surveyList.push(newCheckboxAnswer);


    //Sorting
    this.homePageListItems.sort(function (a, b) {
      return a.frequency.annualized_value - b.frequency.annualized_value;
    });

    //Set unique always at the end
    var uniquefrequency = this.homePageListItems.find(homeItem => homeItem.frequency.annualized_value == 0);
    var indexOfUniqueFrequency = this.homePageListItems.indexOf(uniquefrequency);
    this.homePageListItems.push(this.homePageListItems.splice(indexOfUniqueFrequency, 1)[0]);
    //
  };

  async handleAllUserSurvey(frequency: FrequencyDto, allUserSurvey: UserSurvey[]): Promise<checkboxAnswer> {
    return new Promise(async (resolve, reject) => {
      //Unique
      if (frequency.annualized_value == 0 && allUserSurvey.length >= 1) {
        if (allUserSurvey[0].dateFilled != null) {
          resolve({
            surveyVersion: null,
            isChecked: true,
            isInprogress: false
          });
        } else {
          resolve({
            surveyVersion: null,
            isChecked: false,
            isInprogress: true
          });
        }
      }

      //Daily
      if (frequency.annualized_value == 1) {
        var todayUserSurvey = allUserSurvey.find(us => new Date(us.startDate).toLocaleDateString('en-US') == new Date().toLocaleDateString('en-US'));
        if (todayUserSurvey != null) {
          if (todayUserSurvey.dateFilled != null) {
            resolve({
              surveyVersion: null,
              isChecked: true,
              isInprogress: false
            });
          } else {
            resolve({
              surveyVersion: null,
              isChecked: false,
              isInprogress: true
            });
          }
        }
      }

      //Weekly
      if (frequency.annualized_value == 7) {
        // TODO: implementer weekly
      }

      //Monthly
      if (frequency.annualized_value == 30) {

        var today = new Date();
        var currentMonth = today.getMonth();
        var currentYear = today.getFullYear();

        var monthlyUserSurvey = allUserSurvey.find(us => new Date(us.startDate).getMonth() == currentMonth
          && new Date(us.startDate).getFullYear() == currentYear)
        if (monthlyUserSurvey != null) {
          if (monthlyUserSurvey.dateFilled != null) {
            resolve({
              surveyVersion: null,
              isChecked: true,
              isInprogress: false
            });
          } else {
            resolve({
              surveyVersion: null,
              isChecked: false,
              isInprogress: true
            });
          }
        }
      }

      //Yearly
      if (frequency.annualized_value == 365) {
        var today = new Date();
        var currentYear = today.getFullYear();

        var yearlyUserSurvey = allUserSurvey.find(us => new Date(us.startDate).getFullYear() == currentYear)
        if (yearlyUserSurvey != null) {
          if (yearlyUserSurvey.dateFilled != null) {
            resolve({
              surveyVersion: null,
              isChecked: true,
              isInprogress: false
            });
          } else {
            resolve({
              surveyVersion: null,
              isChecked: false,
              isInprogress: true
            });
          }
        }

      }
      resolve({
        surveyVersion: null,
        isChecked: false,
        isInprogress: false
      });
    });
  }



  // var allUserSurvey: UserSurvey[] = await this.getAllUserSurvey(this.currentUser, latestSurvey.shortName)

  // this.allSurvey.forEach(async survey => {
  //   var latestSurvey: SurveyVersion = await this.getLatestSurvey(survey);
  //   this.allSurveyVersion.push(latestSurvey);

  //   debugger;

  //   var allUserSurvey: UserSurvey[] = await this.getAllUserSurvey(this.currentUser, latestSurvey.shortName)



  //   if (latestSurvey.frequency.annualized_value == 7) {
  //     // if (allUserSurvey.find(us => new Date(us.dateFilled) == new Date()) != null) {
  //     //   this.dailySurvey.push({
  //     //     surveyVersion: latestSurvey,
  //     //     isChecked: true
  //     //   });
  //     // } else {
  //     //   this.dailySurvey.push({
  //     //     surveyVersion: latestSurvey,
  //     //     isChecked: false
  //     //   });
  //     // }
  //   }

  //   if (latestSurvey.frequency.annualized_value == 30) {

  //     var today = new Date();
  //     var currentMonth = new Date().getMonth();
  //     var firstDayOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);

  //     var currenUserSurvey = allUserSurvey.find(us => new Date(us.startDate).getMonth() == currentMonth)

  //     if (currenUserSurvey != null) {
  //       this.monthlysurvey.push({
  //         surveyVersion: latestSurvey,
  //         isChecked: true
  //       });
  //     } else {
  //       this.monthlysurvey.push({
  //         surveyVersion: latestSurvey,
  //         isChecked: false
  //       });
  //     }
  //   }

  //   if (latestSurvey.frequency.annualized_value == 365) {
  //     if (allUserSurvey.find(us => new Date(us.dateFilled).getFullYear() == new Date().getFullYear()) != null) {
  //       this.yearllySurvey.push({
  //         surveyVersion: latestSurvey,
  //         isChecked: true
  //       });
  //     } else {
  //       this.yearllySurvey.push({
  //         surveyVersion: latestSurvey,
  //         isChecked: false
  //       });
  //     }
  //   }

  //   if (latestSurvey.frequency.annualized_value == 0) {
  //     if (allUserSurvey.length != null) {
  //       this.yearllySurvey.push({
  //         surveyVersion: latestSurvey,
  //         isChecked: true
  //       });
  //     } else {
  //       this.yearllySurvey.push({
  //         surveyVersion: latestSurvey,
  //         isChecked: false
  //       });
  //     }
  //   }
  // });

  // async getAllUserSurvey(user: UserDto, surveyShortName: string) {
  //   return this.userSurveyRestService.getAllUserSurveyByUserIdAndSurveyName(surveyShortName, user.id).toPromise();
  // }

  //

  async getLatestSurvey(survey: Survey) {
    return this.surveyRestService.getLatestSurveyVersion(survey.id).toPromise();
  }

  goToProfile() {
    this.router.navigate(['profile']);
  };

  goToPicture() {
    this.router.navigate(['picture']);
  };

  goToMedManager() {
    this.router.navigate(['medManager']);
  };

  goTocomment() { };

}


export interface homePageListItem {
  surveyList: checkboxAnswer[];
  frequency: FrequencyDto;
}

export interface checkboxAnswer {
  surveyVersion: SurveyVersion;
  isChecked: boolean;
  isInprogress: boolean;
}