import { Component, OnInit, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { UserDto } from 'src/app/share/Dto/UserDto';
import { UserSurvey } from 'src/app/share/Dto/UserSurveyDto';
import { UserSurveyRestService } from 'src/app/share/user-survey-rest.service';
import { checkboxAnswer, homePageListItem } from '../home.page';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-homepage-list-item',
  templateUrl: './homepage-list-item.component.html',
  styleUrls: ['./homepage-list-item.component.scss'],
})
export class HomepageListItemComponent implements OnInit {

  @Input() homePageListItem: homePageListItem;
  @Input() user: UserDto;

  aYearLater: string = "";
  constructor(
    private router: Router,
    private userSurveyRestService: UserSurveyRestService,
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.getItems();
  }

  ionViewWillEnter() {
    this.getItems();
  }

  getItems() {

  }


  async goToSurvey(survey: checkboxAnswer) {

    if (survey.surveyVersion.frequency.annualized_value == 1) {
      this.router.navigate(['surveyCalendar', survey.surveyVersion.shortName]);
    } else {
      // this.router.navigate(['survey', { choosedDate: new Date(), surveyShortName: survey.surveyVersion.shortName }])
      var allUserSurvey: UserSurvey[] = await this.getAllUserSurvey(survey.surveyVersion.shortName);
      this.verifyFrequency(allUserSurvey, survey.surveyVersion.shortName)
    }
  }

  async getAllUserSurvey(surveyShortName: string) {
    return this.userSurveyRestService.getAllUserSurveyByUserIdAndSurveyName(surveyShortName, this.user.id).toPromise();
  }

  async verifyFrequency(allUserSurvey: UserSurvey[], surveyShortName: string) {
    if (this.homePageListItem.frequency.annualized_value == 7) {
      this.handleWeekFrequency(allUserSurvey, surveyShortName);
    }
    if (this.homePageListItem.frequency.annualized_value == 30) {
      this.handleMonthFrequency(allUserSurvey, surveyShortName);
    }
    if (this.homePageListItem.frequency.annualized_value == 365) {
      this.handleYearFrequency(allUserSurvey, surveyShortName);
    }
    if (this.homePageListItem.frequency.annualized_value == 0) {
      this.handleUniqueFrequency(allUserSurvey, surveyShortName);
    }
  }


  async handleYearFrequency(allUserSurvey: UserSurvey[], surveyShortName: string) {
    var today = new Date();
    //Sort allusersurvey by date (recent to older)
    if (allUserSurvey == null || allUserSurvey.length == 0) {
      await this.createNewUserSurvey(today, surveyShortName);
    } else {
      allUserSurvey.sort(function (a, b) {
        return b.startDate - a.startDate;
      });
    }

    //get the latest one
    var latestUserSurvey = allUserSurvey[0];

    if (latestUserSurvey.dateFilled == null) {
      await this.handleUserSurvey(latestUserSurvey, today, surveyShortName, "year")
    } else {
      var aYearLater = new Date(latestUserSurvey.dateFilled);
      aYearLater.setFullYear(aYearLater.getFullYear() + 1);
      this.aYearLater = new Date(aYearLater.setHours(0, 0, 0, 0)).toLocaleDateString('en-US');

      // //add a year + 7 day (that create delay 
      // var aYearLaterDelay = new Date(aYearLater);
      // aYearLaterDelay.setDate(aYearLaterDelay.getDate() + 7)

      if (today >= aYearLater) {
        var currenUserSurvey = allUserSurvey.find(us => new Date(us.startDate).getFullYear() == aYearLater.getFullYear())
        if (currenUserSurvey != null) {
          await this.handleUserSurvey(currenUserSurvey, today, surveyShortName, "year")
        } else {
          await this.createNewUserSurvey(today, surveyShortName);
        }
      } else {
        this.translateService.get('Warning.FrequencyWarningSurveyAlreadyCompletedYear', { date: this.aYearLater }).subscribe((res) => {
          alert(res);
        })
      }
    }

    if (latestUserSurvey != null) {
    } else {
      await this.createNewUserSurvey(today, surveyShortName);
    }
  }

  async handleMonthFrequency(allUserSurvey: UserSurvey[], surveyShortName: string) {
    var today = new Date();
    var currentMonth = new Date().getMonth();
    var lastDayOfDelay = new Date(today.getFullYear(), today.getMonth(), 7);
    var firstDayOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);

    var currenUserSurvey = allUserSurvey.find(us => new Date(us.startDate).getMonth() == currentMonth)

    if (today <= lastDayOfDelay) {
      if (currenUserSurvey != null) {
        await this.handleUserSurvey(currenUserSurvey, firstDayOfMonth, surveyShortName, "month")
      }
      if (currenUserSurvey == null) {
        await this.createNewUserSurvey(firstDayOfMonth, surveyShortName);
      }
    } else {
      await this.throwFrequencyWarningOutOfDelay("month");
    }
  }

  async handleWeekFrequency(allUserSurvey: UserSurvey[], surveyShortName: string) {
    // TODO: Implementer week frequency -> demander les regles
  }

  async handleUniqueFrequency(allUserSurvey: UserSurvey[], surveyShortName: string) {
    var today = new Date();

    if (allUserSurvey.length != 0) {
      if (allUserSurvey.length >= 1) {
        this.handleUserSurvey(allUserSurvey[0], today, surveyShortName, "unique");
      }
    } else {
      this.createNewUserSurvey(today, surveyShortName);
    }
  }

  async handleUserSurvey(currentUserSurvey: UserSurvey, choosedDate: Date, surveyShortName: string, frequency: string) {
    if (currentUserSurvey != null && currentUserSurvey.dateFilled == null) {
      this.router.navigate(['survey', { surveyShortName: surveyShortName, choosedDate: currentUserSurvey.startDate, userSurveyId: currentUserSurvey.id }]);
    }
    if (currentUserSurvey != null && currentUserSurvey.dateFilled != null) {
      await this.throwFrequencyWarningSurveyAlreadyCompleted(frequency)
    }
  }

  async createNewUserSurvey(choosedDate: Date, surveyShortName: String) {
    this.router.navigate(['survey', { choosedDate: choosedDate, surveyShortName: surveyShortName }]);
  }



  async throwFrequencyWarningSurveyAlreadyCompleted(frequency: string) {
    if (frequency.toLocaleLowerCase() == "year") {
      this.translateService.get('Warning.FrequencyWarningSurveyAlreadyCompletedYear', { date: this.aYearLater }).subscribe((res) => {
        alert(res);
      })
    }
    if (frequency.toLocaleLowerCase() == "month") {
      this.translateService.get('Warning.FrequencyWarningSurveyAlreadyCompletedMonth').subscribe((res) => {
        alert(res);
      })
    }
    if (frequency.toLocaleLowerCase() == "week") {
      this.translateService.get('Warning.FrequencyWarningSurveyAlreadyCompletedWeek').subscribe((res) => {
        alert(res);
      })
    }
    if (frequency.toLocaleLowerCase() == "unique") {
      this.translateService.get('Warning.FrequencyWarningSurveyAlreadyCompletedUnique').subscribe((res) => {
        alert(res);
      })
    }
  }

  async throwFrequencyWarningOutOfDelay(frequency: string) {
    if (frequency.toLocaleLowerCase() == "year") {
      this.translateService.get('Warning.FrequencyWarningOutOfDelayYear').subscribe((res) => {
        alert(res);
      })
    }
    if (frequency.toLocaleLowerCase() == "month") {
      this.translateService.get('Warning.FrequencyWarningOutOfDelayMonth').subscribe((res) => {
        alert(res);
      })
    }
    if (frequency.toLocaleLowerCase() == "week") {
      this.translateService.get('Warning.FrequencyWarningOutOfDelayWeek').subscribe((res) => {
        alert(res);
      })
    }
    if (frequency.toLocaleLowerCase() == "unique") {
      // TODO: Implementer un message en cas de sondage unique oublié 
      this.translateService.get('Warning.FrequencyWarningSurveyAlreadyCompletedUnique').subscribe((res) => {
        alert(res);
      })
    }
  }


}
