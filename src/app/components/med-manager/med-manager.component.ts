import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Medicament } from '../med-list-search/med-list-search.component';
import { UserRestService } from 'src/app/share/user-rest.service';
import { DrugDto } from 'src/app/share/Dto/DrugDto';
import { PrescriptionRestService } from 'src/app/share/prescription-rest.service';
import { DrugRestService } from 'src/app/share/drug-rest.service';
import { PrescriptionDto } from 'src/app/share/Dto/PrescriptionDto';
import { FrequencyRestService } from 'src/app/share/frequency-rest.service';
import { FrequencyDto } from 'src/app/share/Dto/FrequencyDto';
import { DosageRestService } from 'src/app/share/dosage-rest.service';
import { DosageDto } from 'src/app/share/Dto/DosageDto';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-med-manager',
  templateUrl: './med-manager.component.html',
  styleUrls: ['./med-manager.component.scss']
})
export class MedManagerComponent implements OnInit {

  medBottleIcon = '../../../assets/icons/pills.png';
  deleteIcon = '../../../assets/icons/delete.png';
  plusIcon = '../../../assets/icons/plus.png';
  previousIcon = '../../../assets/icons/previous.png';

  userMedList: PrescriptionDto[] = [];

  allFrequences: FrequencyDto[] = [];
  medDosage = {};

  constructor(
    private router: Router,
    private userRestService: UserRestService,
    private prescriptionRestService: PrescriptionRestService,
    private drugRestService: DrugRestService,
    private frequencyRestService: FrequencyRestService,
    private dosageRestService: DosageRestService,
    private translateService: TranslateService
  ) { }

  ngOnInit(): void {
    this.getItems()
  }

  ionViewWillEnter() {
    this.getItems()
  }

  getItems() {
    this.userMedList = []
    this.userRestService.getAllMePrescription().subscribe((res) => {
      if (res) {
        this.userMedList = res;
        this.userMedList.forEach(med => {
          med.name = med.name.charAt(0).toUpperCase() + med.name.slice(1);
        });
        this.getAllFrequences()
        this.getDosage();
      }
    }), (err) => {
      console.log(err)
    }
  }

  getAllFrequences() {
    this.frequencyRestService.getAllFrequences().subscribe((res) => {
      if (res) {
        this.allFrequences = res;

        for (let index = 0; index < res.length; index++) {
          const frequency = res[index];


          if (this.translateService.currentLang == "en") {
            frequency.name = frequency.en;
          } if (this.translateService.currentLang == "fr") {
            frequency.name = frequency.fr;
          }
        }
        this.allFrequences = res
        var uniqueIndex = this.allFrequences.indexOf(this.allFrequences.find(f => f.annualized_value == 0));
        this.allFrequences.splice(uniqueIndex, 1);



        this.allFrequences.sort((frequency1, frequency2) => frequency1.annualized_value - frequency2.annualized_value);
      }
    }), (err) => {
      console.log(err)
    }
  }

  getFrequency(index: number): string {
    return this.allFrequences.find(f => f.id == this.userMedList[index].frequencyId).name
  }

  getDosage() {
    this.medDosage = {}
    this.userMedList.forEach(med => {
      this.dosageRestService.getDosageById(med.dosageId).subscribe((res: DosageDto) => {
        this.medDosage[med.dosageId] = res.description
      })
    });
  }


  addMed() {
    this.router.navigate(['medListSearch']);
  }

  editMed(index: number): any {
    const drugId = this.userMedList[index].drugId;
    this.drugRestService.getDrugById(drugId).subscribe((res) => {
      this.router.navigate(['medInformation'], {
        state: {
          medicament: res,
          prescription: this.userMedList[index].id
        }
      });
    }), (err) => {
      console.log(err)
    }
  }

  confirmDelete(index: number) {
    this.router.navigate(['medDeleteInformation'], {
      state: {
        medicament: this.userMedList[index],
      }
    });
  }

  goToAccount() {
    this.router.navigate([''])
  }

}

