import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedManagerComponent } from './med-manager.component';

describe('MedManagerComponent', () => {
  let component: MedManagerComponent;
  let fixture: ComponentFixture<MedManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
