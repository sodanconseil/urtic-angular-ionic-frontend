import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

// FORM
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserRestService } from 'src/app/share/user-rest.service';
import { EthnicityRestService } from 'src/app/share/ethnicity-rest.service';
import { UserDto } from 'src/app/share/Dto/UserDto';
import { EthnicityDto } from 'src/app/share/Dto/EthnicityDto';
import { MeUserDto } from 'src/app/share/Dto/MeUserDto';
import { InputElement } from './InputElement';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/keycloak/auth.service';

@Component({
  selector: 'app-complete-profile-form',
  templateUrl: './complete-profile-form.component.html',
  styleUrls: ['./complete-profile-form.component.scss']
})

export class CompletProfileFormComponent implements OnInit {

  medBottleIcon = '../../../assets/icons/pills.png';
  deleteIcon = '../../../assets/icons/delete.png';
  plusIcon = '../../../assets/icons/plus.png';
  previousIcon = '../../../assets/icons/previous.png';
  nextIcon = '../../../assets/icons/next.png';
  postIcon = '../../../assets/icons/post.png';
  logoutImage = "../../../assets/icons/logout.png";


  keyclaokUserId: string = "139902a0-a237-4dd4-acf2-6e2da1326533"
  currentUser: UserDto;
  ethlist: EthnicityDto[];
  allLanguage: string[] = [];

  language = new InputElement();
  phone = new InputElement();
  birthDate = new InputElement();
  ethnicity = new InputElement();
  birthGender = new InputElement();
  civilNum = new InputElement();
  appNum = new InputElement();
  street = new InputElement();
  city = new InputElement();
  state = new InputElement();
  postalCode = new InputElement();

  requiredInputs = [
    this.birthDate,
    this.ethnicity,
    this.birthGender,
    this.civilNum,
    this.street,
    this.city,
    this.state,
    this.postalCode,
  ]

  allInputs = [
    this.phone,
    this.birthDate,
    this.ethnicity,
    this.birthGender,
    this.civilNum,
    this.appNum,
    this.street,
    this.city,
    this.state,
    this.postalCode,
    this.language,
  ]

  provinceList: any = [
    'Alberta',
    'Colombie-Britannique',
    'Île-du-Prince-Édouard',
    'Nouveau-Brunswick',
    'Manitoba',
    'Nouvelle-Écosse',
    'Nunavut',
    'Ontario',
    'Québec',
    'Saskatchewan',
    'Terre-Neuve-et-Labrador',
    'Territoires du Nord-Ouest',
    'Yukon',
  ];
  constructor(
    public modalCtrl: ModalController,
    private router: Router,
    private userRestService: UserRestService,
    private ethnicityRestService: EthnicityRestService,
    private translateService: TranslateService,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.getUser();
    this.getEthnicity();
    this.translateService.use(this.translateService.getDefaultLang())
    this.allLanguage = this.translateService.getLangs();
  }

  getUser() {
    this.userRestService.get(this.keyclaokUserId).subscribe((value) => {
      this.currentUser = value;
      localStorage.setItem("userId", this.keyclaokUserId);
    });
  }

  getEthnicity() {
    this.ethnicityRestService.getAll().subscribe((value) => {
      this.ethlist = value;
      this.updateEthnicityLang(this.ethlist);
    })
  }

  updateEthnicityLang(ethlist: EthnicityDto[]) {
    ethlist.forEach(ethnicity => {
      if (this.translateService.currentLang == "en") {
        ethnicity.name = ethnicity.en;
      } if (this.translateService.currentLang == "fr") {
        ethnicity.name = ethnicity.fr;
      }
    });
    return ethlist
  }

  updateLanguage() {
    this.translateService.use(this.language.input);
    this.getEthnicity();
  }

  createEthnicity() {
    this.ethnicity.input.forEach(eth => {
      const ethToAdd = this.ethlist.find(e => e.name == eth)
      this.userRestService.createMeEthnicity(ethToAdd.id).subscribe((res) => {
        if (res) {
          this.goToMedicalHistorySurvey();
        }
      });
    });
  }

  updatePostaleCode() {
    this.postalCode.input = this.postalCode.input.toUpperCase();
  }

  verifyRequiredFields() {
    // resets the previous values
    for (const key of Object.keys(this.allInputs)) {
      this.allInputs[key].class = "";
    }

    let invalidInputElements = []

    // from https://stackoverflow.com/questions/16699007/regular-expression-to-match-standard-10-digit-phone-number
    const phone: string = this.phone.input;
    if (phone && !(phone.match("^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]?\\d{3}[\\s.-]?\\d{4}$"))) {
      invalidInputElements.push(this.phone);
    }

    for (const key of Object.keys(this.requiredInputs)) {
      if (!this.requiredInputs[key].input) {
        invalidInputElements.push(this.requiredInputs[key]);
      }
    }

    // from https://stackoverflow.com/questions/15774555/efficient-regex-for-canadian-postal-code-function
    const postalCode: string = this.postalCode.input;
    if (!(postalCode && postalCode.match("^[ABCEGHJ-NPRSTVXY]\\d[ABCEGHJ-NPRSTV-Z][ -]?\\d[ABCEGHJ-NPRSTV-Z]\\d$"))) {
      invalidInputElements.push(this.postalCode);
    }

    invalidInputElements.forEach(element => element.class = "invalidInput");
    void window.innerHeight;

    const requiredFieldsFilled = invalidInputElements.length === 0

    // workaround for restarting the css animation
    if (!requiredFieldsFilled)
      setTimeout(() => invalidInputElements[0].class = "invalidInputWithShake", 100)

    return requiredFieldsFilled;
  }

  postUser(): void {
    if (this.verifyRequiredFields()) {

      const user: UserDto = {
        birthday: this.birthDate.input,
        phoneNumber: this.phone.input,
        gender: this.birthGender.input,
        prefferedLanguage: this.language.input,
        description: `${this.appNum.input}-${this.civilNum.input} ${this.street.input}, ${this.city.input}, ${this.state.input}, ${this.postalCode.input}`,
      };

      this.userRestService.post(user).subscribe((returnedUser) => {
        if (returnedUser) {
          this.createEthnicity();
        }
      })
    }
  }

  goToMedicalHistorySurvey() {
    this.router.navigate(['survey', { choosedDate: new Date(), surveyShortName: "HM" }]).then(() => {
      this.modalCtrl.dismiss();
    })
  }

  closeCompleteProfile() {
    this.modalCtrl.dismiss();
  }

  async logout() {
    this.authService.keycloakLogout(true)
    return Promise.resolve().then(() => {
      window.localStorage.clear();
      window.localStorage.setItem('isLoggedIn', JSON.stringify(false));
      window.localStorage.setItem('completeProfileCompleted', JSON.stringify(false));
      this.currentUser = null;
      window.location.reload();
    });
  }
}
