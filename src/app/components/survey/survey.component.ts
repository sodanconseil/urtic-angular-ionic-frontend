import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AnswerChoice } from 'src/app/share/Dto/AnswerChoiceDto';
import { QuestionVersion } from 'src/app/share/Dto/QuestionVersionDto';
import { SubQuestionCondition } from 'src/app/share/Dto/subQuestionConditionDto';
import { Survey } from 'src/app/share/Dto/SurveyDto';
import { SurveyVersion } from 'src/app/share/Dto/SurveyVersionDto';
import { UserAnswer } from 'src/app/share/Dto/UserAnswerDto';
import { UserDto } from 'src/app/share/Dto/UserDto';
import { UserQuestion } from 'src/app/share/Dto/UserQuestionDto';
import { UserSurvey } from 'src/app/share/Dto/UserSurveyDto';
import { SubQuestionConditionRestService } from 'src/app/share/sub-question-condition-rest.service';
import { SurveyRestService } from 'src/app/share/survey-rest.service';
import { UserAnswerRestService } from 'src/app/share/user-answer-rest.service';
import { UserQuestionRestService } from 'src/app/share/user-question-rest.service';
import { UserRestService } from 'src/app/share/user-rest.service';
import { UserSurveyRestService } from 'src/app/share/user-survey-rest.service';




@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss'],
})
export class SurveyComponent implements OnInit {
  previousIcon = '../../../assets/icons/previous.png';
  nextIcon = '../../../assets/icons/next.png';

  currentUser: UserDto;
  choosedDate: any = new Date();
  surveyShortName: string = "";
  userSurveyIdFromChoosedDate: string = "";
  currentSurveyVersion: SurveyVersion;
  allQuestions: QuestionVersion[] = [];
  allSubQuestionCondition: SubQuestionCondition[] = [];
  currentPageIndex = 1;
  currentUserSurvey: UserSurvey = {};
  currentQuestion: QuestionVersion = { text: "", info: "", questionType: { name: "" }, possibleAnswers: [{}] };
  currentAnswers: AnswerChoice[] = [];
  disableNextButton: boolean = true

  hideBackButton: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private surveyRestService: SurveyRestService,
    private userSurveyRestService: UserSurveyRestService,
    private userRestService: UserRestService,
    private userQuestionRestService: UserQuestionRestService,
    private subQuestionConditionRestService: SubQuestionConditionRestService,
    private userAnswerRestService: UserAnswerRestService,
  ) { }

  ngOnInit() {

    this.getItems();
  }

  async getItems() {

    //Get and Init user
    const user = await this.getUser();
    this.currentUser = user

    //Init Params from calendar
    await this.getRouterParams();

    //Get and Init survey 
    const surveyList: Survey[] = await this.getSurvey();
    const survey = surveyList.find(survey => survey.shortName == this.surveyShortName);
    const surveyVersion: SurveyVersion = await this.getSurveyVersion(survey.id);
    this.currentSurveyVersion = surveyVersion;

    //init userSurvey
    await this.initQuestions(surveyVersion)

    // if (this.surveyShortName == "HM") {
    //   this.hideBackButton = true;
    // }

    // init user survey
    if (this.userSurveyIdFromChoosedDate != null) {
      this.getAlreadyStartedUserSurvey();
    } else {
      this.postUserSurvey();
    }
  }

  async getUser() {
    return await this.userRestService.getMe().toPromise();
  }

  getRouterParams() {
    return new Promise((resolve, reject) => {
      this.surveyShortName = this.route.snapshot.paramMap.get("surveyShortName");
      this.choosedDate = this.route.snapshot.paramMap.get("choosedDate");
      this.userSurveyIdFromChoosedDate = this.route.snapshot.paramMap.get("userSurveyId");
      resolve(null);
    });
  }

  async getSurvey() {
    return await this.surveyRestService.getAll().toPromise();
  }

  async getSurveyVersion(surveyId: string) {
    return await this.surveyRestService.getLatestSurveyVersion(surveyId).toPromise();
  }

  async initQuestions(surveyVersion: SurveyVersion) {
    this.allQuestions = surveyVersion.questionVersions.filter(qv => qv.hidden != true);
    this.allQuestions = this.allQuestions.filter(qv => qv.subQuestion != true)
    this.currentQuestion = this.allQuestions.find(q => q.question_order == this.currentPageIndex);

    this.allQuestions.sort(function (questionA, questionB) {
      return questionA.verificationOrder - questionB.verificationOrder;
    });


    //Sub question condition
    var conditions: SubQuestionCondition[] = await this.getSubQuestionCondition();
    this.allSubQuestionCondition = conditions;

    //     if (this.currentQuestion.question_order == this.allQuestions.length) {
    //       document.getElementById("nextButton_label").innerHTML = "Terminer"
    //     }
  }

  postUserSurvey() {
    var newUserSurvey: UserSurvey = {
      name: "",
      survey_version_Id: this.currentSurveyVersion.id,
      user_id: this.currentUser.id,
      startDate: new Date(this.choosedDate).toISOString()
    }

    this.userSurveyRestService.postUserSurvey(newUserSurvey).subscribe((userSurvey: UserSurvey) => {
      if (userSurvey) {
        this.currentUserSurvey = userSurvey;
      }
    }, (err) => {
      console.log("The userSurvey  " + newUserSurvey + "couldn't be posted: " + err)
    })
  }

  async getAlreadyStartedUserSurvey() {
    const currentUserSurvey = await this.getUserSurvey()
    this.currentUserSurvey = currentUserSurvey;
    //get all the answers
    const allAnsweredQuestions = await this.getAllAnsweredQuestionFromUserSurvey()
    this.filterSurveyQuestion(allAnsweredQuestions);
  }

  async getUserSurvey() {
    return this.userSurveyRestService.get(this.userSurveyIdFromChoosedDate).toPromise();
  }

  async getAllAnsweredQuestionFromUserSurvey() {
    return this.userQuestionRestService.getAllUserQuestionFromUserSurvey(this.currentUserSurvey.id).toPromise();
  }

  async filterSurveyQuestion(allAnsweredQuestion: UserQuestion[]) {
    var allQuestionFromDb = this.currentSurveyVersion.questionVersions;

    var allQuestionFromDbFiltered = allQuestionFromDb.filter(qv => qv.hidden != true).map((question) => question);

    var conditionIsMet: boolean;
    for (let index = 0; index < allQuestionFromDbFiltered.length; index++) {
      const question = allQuestionFromDbFiltered.find(q => q.verificationOrder == index + 1);

      //get all the answers of a question
      var currentUserQuestion: UserQuestion = allAnsweredQuestion.find(uq => uq.questionAnswered == question.id);
      if (question.subQuestion == true && conditionIsMet == false && currentUserQuestion == null) {
        continue
      }
      var answers = await this.getAnswers(currentUserQuestion.id);

      //Get the condition
      var currentCondition: SubQuestionCondition = this.allSubQuestionCondition.find(c => c.questionVersion == this.currentQuestion.id);


      // check if conditions is met
      conditionIsMet = await this.checkIfSubConditionIsMet(answers, currentCondition);

      if (conditionIsMet) {
        this.doActionWhenConditionIsMet(currentCondition);
      } else {
        this.doActionWhenConditionIsNotMet(currentCondition);
      }

      this.currentPageIndex++
      this.currentQuestion = this.allQuestions.find(q => q.question_order == this.currentPageIndex);
      this.currentAnswers = [];
      this.disableNextButton = true;
    }

    this.allQuestions.sort(function (questionA, questionB) {
      return questionA.verificationOrder - questionB.verificationOrder;
    });
  }

  async getAnswers(userQuestionId: string) {
    return new Promise(async (resolve, reject) => {
      var allUserAnswers: UserAnswer[] = await this.getAllUserAnswerByUserQuestionId(userQuestionId);
      var answers: AnswerChoice[] = [];
      allUserAnswers.forEach(userAnswer => {
        answers.push({ name: "", id: userAnswer.answerChoice_id })
        resolve(answers)
      });
    });

  }

  async getAllUserAnswerByUserQuestionId(userQuestionId: string) {
    return this.userAnswerRestService.getAllByUserQuestionId(userQuestionId).toPromise();
  }

  async nextQuestion() {
    await this.postAnswer().then(() => {
      this.currentAnswers.forEach(answer => {
        if (answer.endSurvey == true) {
          this.editUserSurvey(true);
        }
      });
      if (this.currentPageIndex == this.allQuestions.length) {
        this.editUserSurvey(true);
      } else {
        this.currentPageIndex++
        this.currentQuestion = this.allQuestions.find(q => q.question_order == this.currentPageIndex);
        this.currentAnswers = [];
        this.disableNextButton = true;

        //         if (this.currentQuestion.question_order == this.allQuestions.length) {
        //           document.getElementById("nextButton_label").innerHTML = "Terminer"
        //         }
      }
    });
  }


  editUserSurvey(finish: boolean = false) {
    this.currentUserSurvey.dateFilled = new Date().toISOString();
    this.userSurveyRestService.editUserSurvey(this.currentUserSurvey.id, this.currentUserSurvey).toPromise()
      .then((editedUserSurvey) => {
        this.currentUserSurvey = editedUserSurvey;
        if (finish) {
          this.router.navigate(['']);
        }
      })
  }


  async postAnswer() {
    this.userSurveyRestService.postUserAnswer(this.currentUserSurvey.id, this.currentAnswers).toPromise();
  }

  updateCurrentAnswer(val: any) {

    this.currentAnswers = val.answers
    this.disableNextButton = val.disableNextButton;
    this.checkForSubQuestion(val.answers);
  }


  async getSubQuestionCondition() {
    return this.subQuestionConditionRestService.getAllBySurveyVersion(this.currentSurveyVersion.id).toPromise();
  }

  async checkForSubQuestion(answers: any) {
    //Get the condition
    var currentCondition: SubQuestionCondition = this.allSubQuestionCondition.find(c => c.questionVersion == this.currentQuestion.id);

    // check if conditions is met
    var conditionIsMet = await this.checkIfSubConditionIsMet(answers, currentCondition);

    if (conditionIsMet) {
      this.doActionWhenConditionIsMet(currentCondition);
    } else {
      this.doActionWhenConditionIsNotMet(currentCondition);
    }
  }

  async checkIfSubConditionIsMet(answers: any, currentCondition: SubQuestionCondition): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (currentCondition != null) {
        var subQuestionConditionsAnswers = currentCondition.answerChoices.map((answer) => answer);
        answers.forEach(answer => {
          if (subQuestionConditionsAnswers.find(suba => suba.id == answer.id)) {
            // var questionIndex = subQuestionConditionsAnswers.findIndex(suba => suba.answer_order == answer.answer_order)
            var questionIndex = subQuestionConditionsAnswers.findIndex(suba => suba.id == answer.id)
            subQuestionConditionsAnswers.splice(questionIndex, 1);
          }
        });

        if (subQuestionConditionsAnswers.length == 0 || null) {
          resolve(true);
        } else {
          resolve(false);
        }
      } else {
        resolve(false);
      }
    });
  }


  async doActionWhenConditionIsMet(currentCondition: SubQuestionCondition) {
    //get the question
    var currentSubQuestion = this.currentSurveyVersion.questionVersions.find(qv => qv.id == currentCondition.subQuestionVersion)

    //fix the index
    currentSubQuestion.question_order = this.currentQuestion.question_order + 1;

    //fix next question index
    if (this.allQuestions.includes(currentSubQuestion) == false) {
      this.allQuestions.forEach(question => {
        if (question.question_order >= currentSubQuestion.question_order) {
          question.question_order++;
        }
      });

      //Add the sub question in the list
      this.allQuestions.push(currentSubQuestion)
    }
  }

  doActionWhenConditionIsNotMet(currentCondition: SubQuestionCondition) {
    if (currentCondition != null) {
      var currentSubQuestion = this.currentSurveyVersion.questionVersions.find(qv => qv.id == currentCondition.subQuestionVersion)

      if (this.allQuestions.includes(currentSubQuestion) == true) {
        //find index of subquestion and remove from list 
        var subQestionIndex = this.allQuestions.findIndex(q => q.id == currentSubQuestion.id);
        this.allQuestions.splice(subQestionIndex, 1);

        //Fix other question's index
        this.allQuestions.forEach(question => {
          if (question.question_order > currentSubQuestion.question_order) {
            question.question_order--;
          }
        });

        //fix the index of subQuestion
        currentSubQuestion.question_order = 0;
      }
    }
  }

  goToAccount() {
    // if (this.surveyShortName == "HM") {
    //   this.router.navigate(['medManager']);
    // } else {
    //   this.router.navigate(['']);
    // }
    this.router.navigate(['']);
  }

}

// export interface SelectedAnswer {
//   answer: AnswerChoice;
//   page: number
// }

// function delay(ms: number) {
//   return new Promise(resolve => setTimeout(resolve, ms));
// }

