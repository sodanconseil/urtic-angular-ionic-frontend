import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AnswerChoice } from 'src/app/share/Dto/AnswerChoiceDto';
import { QuestionVersion } from 'src/app/share/Dto/QuestionVersionDto';
import { SubQuestionCondition } from 'src/app/share/Dto/subQuestionConditionDto';
import { SurveyVersion } from 'src/app/share/Dto/SurveyVersionDto';
import { UserAnswer } from 'src/app/share/Dto/UserAnswerDto';
import { UserQuestion } from 'src/app/share/Dto/UserQuestionDto';
import { QuestionVersionRestService } from 'src/app/share/question-version-rest.service';
import { SubQuestionConditionRestService } from 'src/app/share/sub-question-condition-rest.service';
import { SurveyRestService } from 'src/app/share/survey-rest.service';
import { UserAnswerRestService } from 'src/app/share/user-answer-rest.service';
import { UserQuestionRestService } from 'src/app/share/user-question-rest.service';
import { UserSurveyRestService } from 'src/app/share/user-survey-rest.service';


@Component({
  selector: 'app-edit-survey-answer',
  templateUrl: './edit-survey-answer.component.html',
  styleUrls: ['./edit-survey-answer.component.scss'],
})
export class EditSurveyAnswerComponent implements OnInit {
  nextIcon = '../../../assets/icons/next.png';
  previousIcon = '../../../assets/icons/previous.png';

  routeAnswerId: string;
  routeQuestionVersionId: string;
  routeSurveyId: string;
  routeSurveyShortName: string;
  routeUserSurveyId: string;
  routeUserQuestion: string;


  //   currentAnswer: UserAnswer;


  currentQuestionVersion: QuestionVersion;
  allUserQuestion: UserQuestion[] = [];
  allQuestions: QuestionVersion[] = [];
  currentPageIndex: number = 1;

  currentSurveyVersion: SurveyVersion;
  allSubQuestionCondition: SubQuestionCondition[] = [];

  verifyOrder: number = 0;

  newAnswer: AnswerChoice[] = [];
  disableNextButton: boolean = true;

  subConditionIsMet: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private questionVersionRestService: QuestionVersionRestService,
    private userQuestionRestService: UserQuestionRestService,
    private userAnswerRestService: UserAnswerRestService,
    private surveyRestService: SurveyRestService,
    private subQuestionConditionRestService: SubQuestionConditionRestService,
    private userSurveyRestService: UserSurveyRestService,
  ) { }

  async ngOnInit() {
    await this.getItems();
  }

  //   async ionViewDidEnter() {
  //     await this.getItems();
  //   }

  async getItems() {
    await this.getRouteParams();
    this.allUserQuestion = await this.getAllUserQuestion();

    this.currentSurveyVersion = await this.getSurveyVersion();

    var conditions: SubQuestionCondition[] = await this.getSubQuestionCondition();
    this.allSubQuestionCondition = conditions;

    this.currentQuestionVersion = await this.getQuestionVersion();
    await this.initQuestion();
  }

  async getRouteParams() {
    return new Promise((resolve, reject) => {
      this.routeAnswerId = this.route.snapshot.paramMap.get("choosedAnswerId");
      this.routeQuestionVersionId = this.route.snapshot.paramMap.get("questionVersion");
      this.routeSurveyId = this.route.snapshot.paramMap.get("surveyId");
      this.routeSurveyShortName = this.route.snapshot.paramMap.get("surveyShortName");
      this.routeUserSurveyId = this.route.snapshot.paramMap.get("currentUserSurveyId");
      this.routeUserQuestion = this.route.snapshot.paramMap.get("userQuestion");
      resolve(null);
    });
  }

  async getAllUserQuestion() {
    return this.userQuestionRestService.getAllUserQuestionFromUserSurvey(this.routeUserSurveyId).toPromise();
  }

  async getSurveyVersion() {
    return this.surveyRestService.getLatestSurveyVersion(this.routeSurveyId).toPromise();
  }


  async getSubQuestionCondition() {
    return this.subQuestionConditionRestService.getAllBySurveyVersion(this.currentSurveyVersion.id).toPromise();
  }

  async getQuestionVersion() {
    return this.questionVersionRestService.getById(this.routeQuestionVersionId).toPromise();
  }

  async initQuestion() {
    return new Promise((resolve, reject) => {
      this.allQuestions.push(this.currentQuestionVersion);
      this.verifyOrder = this.currentQuestionVersion.question_order;
      resolve(true);
    });
  }

  updateCurrentAnswer(val: any) {
    this.newAnswer = val.answers
    this.disableNextButton = val.disableNextButton;

    this.checkForSubQuestion(val.answers);
  }

  async checkForSubQuestion(answers: any) {
    //Get the condition
    var currentCondition: SubQuestionCondition = this.allSubQuestionCondition.find(c => c.questionVersion == this.currentQuestionVersion.id);

    // check if conditions is met
    var conditionIsMet = await this.checkIfSubConditionIsMet(answers, currentCondition);

    if (conditionIsMet) {
      this.subConditionIsMet = true;
      this.doActionWhenConditionIsMet(currentCondition);
    } else {
      this.subConditionIsMet = false;
      this.doActionWhenConditionIsNotMet(currentCondition);
    }
  }

  async checkIfSubConditionIsMet(answers: any, currentCondition: SubQuestionCondition) {
    return new Promise((resolve, reject) => {
      if (currentCondition != null) {
        var subQuestionConditionsAnswers = currentCondition.answerChoices.map((answer) => answer);
        answers.forEach(answer => {
          if (subQuestionConditionsAnswers.find(suba => suba.id == answer.id)) {
            // var questionIndex = subQuestionConditionsAnswers.findIndex(suba => suba.answer_order == answer.answer_order)
            var questionIndex = subQuestionConditionsAnswers.findIndex(suba => suba.id == answer.id)
            subQuestionConditionsAnswers.splice(questionIndex, 1);
          }
        });

        if (subQuestionConditionsAnswers.length == 0 || null) {
          resolve(true);
        } else {
          resolve(false);
        }
      } else {
        resolve(false);
      }
    });
  }

  async doActionWhenConditionIsMet(currentCondition: SubQuestionCondition) {
    //get the question
    var currentSubQuestion = this.currentSurveyVersion.questionVersions.find(qv => qv.id == currentCondition.subQuestionVersion)

    //fix the index
    currentSubQuestion.question_order = this.currentQuestionVersion.question_order + 1;

    //fix next question index
    if (this.allQuestions.includes(currentSubQuestion) == false) {
      //Add the sub question in the list
      this.allQuestions.push(currentSubQuestion)
    }
  }

  doActionWhenConditionIsNotMet(currentCondition: SubQuestionCondition) {
    if (currentCondition != null) {
      var currentSubQuestion = this.currentSurveyVersion.questionVersions.find(qv => qv.id == currentCondition.subQuestionVersion)

      if (this.allQuestions.includes(currentSubQuestion) == true) {
        //find index of subquestion and remove from list 
        var subQestionIndex = this.allQuestions.findIndex(q => q.id == currentSubQuestion.id);
        this.allQuestions.splice(subQestionIndex, 1);

        //Fix other question's index
        this.allQuestions.forEach(question => {
          if (question.question_order > currentSubQuestion.question_order) {
            question.question_order--;
          }
        });

        //fix the index of subQuestion
        currentSubQuestion.question_order = 0;
      }
    }
  }

  async nextQuestion() {
    await this.editAnswer().then(() => {
      if (this.currentPageIndex == this.allQuestions.length) {
        this.goToAccount();
      } else {
        this.currentPageIndex++;
        this.verifyOrder++;
        this.currentQuestionVersion = this.allQuestions.find(q => q.question_order == this.verifyOrder);
        this.newAnswer = [];
        this.subConditionIsMet = false;
        this.disableNextButton = true;

        //         if (this.currentQuestion.question_order == this.allQuestions.length) {
        //           document.getElementById("nextButton_label").innerHTML = "Terminer"
        //         }
      }
    });
  }


  async editAnswer() {
    var newAnswersList: UserAnswer[] = []

    var currentUserQuestion = this.allUserQuestion.find(uq => uq.questionAnswered == this.currentQuestionVersion.id);
    if (currentUserQuestion == null && this.currentQuestionVersion.subQuestion == true) {
      this.newAnswer.forEach(element => {

      });
      return await this.postAnswer()
    } else {
      for (let index = 0; index < this.newAnswer.length; index++) {
        const a = this.newAnswer[index];
        const editedAnswer: UserAnswer = {
          name: "",
          answerChoice_id: a.id,
          stringAnswer: a.text,
          intAnswer: a.value != null ? a.value : null,
          userQuestion: currentUserQuestion,
          dateAnswer: new Date(),
          supplementInputAnswer: a.supplementInputAnswer != null ? a.supplementInputAnswer : null,
          subConditionIsMet: this.subConditionIsMet
        }
        newAnswersList.push(editedAnswer)
      }

      return this.userAnswerRestService.edit(this.routeAnswerId, newAnswersList).toPromise()
    }
  }

  async postAnswer() {
    this.userSurveyRestService.postUserAnswer(this.routeUserSurveyId, this.newAnswer).toPromise();
  }

  goToAccount() {
    this.router.navigate(['surveyAnswerManager', this.routeUserSurveyId, { surveyShortName: this.routeSurveyShortName }]);
  }
}

// export interface SubQuestionAnswerList {
//   subQuestionId: string,
//   answers: UserAnswer[]
// }

// export interface MainQuestionAnswerList {
//   mainQuestionId: string,
//   answers: UserAnswer[]
// }

// export interface NewAnswerDto {
//   mainQuestionAnswer: MainQuestionAnswerList,
//   subQuestionAnswerList: SubQuestionAnswerList[]
// }