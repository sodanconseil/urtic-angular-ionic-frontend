import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AnswerChoice } from 'src/app/share/Dto/AnswerChoiceDto';
import { QuestionVersion } from 'src/app/share/Dto/QuestionVersionDto';
import { Survey } from 'src/app/share/Dto/SurveyDto';
import { SurveyVersion } from 'src/app/share/Dto/SurveyVersionDto';
import { UserAnswer } from 'src/app/share/Dto/UserAnswerDto';
import { UserQuestion } from 'src/app/share/Dto/UserQuestionDto';
import { UserSurvey } from 'src/app/share/Dto/UserSurveyDto';
import { SurveyRestService } from 'src/app/share/survey-rest.service';
import { UserAnswerRestService } from 'src/app/share/user-answer-rest.service';
import { UserQuestionRestService } from 'src/app/share/user-question-rest.service';
import { UserSurveyRestService } from 'src/app/share/user-survey-rest.service';
// import { ActivatedRoute, Route, Router } from '@angular/router';
// import { QuestionVersion } from 'src/app/share/Dto/QuestionVersionDto';
// import { Survey } from 'src/app/share/Dto/SurveyDto';
// import { SurveyVersion } from 'src/app/share/Dto/SurveyVersionDto';
// import { UserAnswer } from 'src/app/share/Dto/UserAnswerDto';
// import { UserQuestion } from 'src/app/share/Dto/UserQuestionDto';
// import { UserSurvey } from 'src/app/share/Dto/UserSurveyDto';
// import { SurveyRestService } from 'src/app/share/survey-rest.service';
// import { UserAnswerRestService } from 'src/app/share/user-answer-rest.service';
// import { UserQuestionRestService } from 'src/app/share/user-question-rest.service';
// import { UserSurveyRestService } from 'src/app/share/user-survey-rest.service';

@Component({
  selector: 'app-survey-answer-manager',
  templateUrl: './survey-answer-manager.component.html',
  styleUrls: ['./survey-answer-manager.component.scss'],
})
export class SurveyAnswerManagerComponent implements OnInit {
  previousIcon = '../../../assets/icons/previous.png';
  editIcon = '../../../assets/icons/edit.png'

  surveyShortName: string = '';
  currentSurvey: Survey;
  currentSurveyVersion: SurveyVersion;
  allQuestions: QuestionVersion[] = [];

  allUserQuestions: UserQuestion[];
  allAnswerStringValue: AnswerStringValue[] = [];

  currentUserSurveyId: string = "";

  // currentUserSurvey: UserSurvey;
  //   allAnswers: UserAnswer[] = [];

  constructor(
    private userQuestionRestService: UserQuestionRestService,
    private userSurveyRestService: UserSurveyRestService,
    private surveyRestService: SurveyRestService,
    private userAnswerRestService: UserAnswerRestService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  async ngOnInit() {
    await this.getItems();
  }

  async ionViewWillEnter() {
    await this.getItems();
    await this.getAnswersStringValue();
  }

  async getItems() {
    await this.getRouteParams();

    const surveyList: Survey[] = await this.getSurvey();
    const survey = surveyList.find(survey => survey.shortName == this.surveyShortName);
    this.currentSurvey = survey;
    this.currentSurveyVersion = await this.getSurveyVersion(survey.id);

    this.allUserQuestions = await this.getAllUserQuestion();
    await this.initQuestion();

  }

  getRouteParams() {
    this.surveyShortName = this.route.snapshot.paramMap.get("surveyShortName");
    this.currentUserSurveyId = this.route.snapshot.paramMap.get("userSurveyId");
  }

  async getSurvey() {
    return await this.surveyRestService.getAll().toPromise();
  }

  async getSurveyVersion(surveyId: string) {
    return await this.surveyRestService.getLatestSurveyVersion(surveyId).toPromise();
  }

  async getAllUserQuestion() {
    return this.userQuestionRestService.getAllUserQuestionFromUserSurvey(this.currentUserSurveyId).toPromise()
  }

  async initQuestion() {
    this.allQuestions = this.currentSurveyVersion.questionVersions;
    var questionsList: QuestionVersion[] = this.currentSurveyVersion.questionVersions
      .filter(qv => qv.hidden != true)
      .map((question: QuestionVersion) => question);
    var filteredQuestionList: QuestionVersion[] = [];

    for (let index = 0; index < this.allUserQuestions.length; index++) {
      var currentUserQuestion = this.allUserQuestions[index];

      // find the question
      const question: QuestionVersion = questionsList.find(q => q.id == currentUserQuestion.questionAnswered);
      if (question != null) {
        filteredQuestionList.push(question);
      }
    }
    this.allQuestions = filteredQuestionList;
    this.allQuestions.sort(function (questionA, questionB) {
      return questionA.verificationOrder - questionB.verificationOrder;
    });
  }

  // async getUserSurvey() {
  //   const userSurveyId = this.route.snapshot.paramMap.get("userSurveyId");
  //   return this.userSurveyRestService.get(userSurveyId).toPromise();
  // }


  //   async getAllAnswers() {
  //     const userQuestionList = await this.getAllUserQuestion();
  //     var userAnswerList: UserAnswer[] = []
  //     for (const userQuestion of userQuestionList) {
  //       const answers = await this.getUserAnswerFromUserQuestion(userQuestion);
  //       for (const answer of answers) {
  //         userAnswerList.push(answer)
  //       }
  //     }
  //     this.allAnswers = userAnswerList;

  //   }




  async getAnswersStringValue() {
    this.allAnswerStringValue = [];
    for (let index = 0; index < this.allUserQuestions.length; index++) {
      const userQuestion = this.allUserQuestions[index];

      var verificationOrder = this.allQuestions.find(q => q.id == userQuestion.questionAnswered).verificationOrder;

      var answers: UserAnswer[] = await this.getUserAnswerFromUserQuestion(userQuestion);
      var htmlString = "";
      var finalString = "";

      var supplementInputString;
      answers.forEach(a => {
        var currentQuestionVersion: QuestionVersion = this.allQuestions.find(qv => qv.id == userQuestion.questionAnswered);
        var currentAnswerChoice: AnswerChoice = currentQuestionVersion.possibleAnswers.find(pa => pa.id == a.answerChoice_id);

        if (a.supplementInputAnswer != null && currentQuestionVersion.questionType.name != "String value") {
          htmlString = htmlString + a.stringAnswer + "\n\n" + currentAnswerChoice.supplementInputText + "\n *" + a.supplementInputAnswer + "\n\n "
        } else {
          htmlString = htmlString + a.stringAnswer + ",\n\n"
        }
        finalString = htmlString.slice(0, -3);

      });

      this.allAnswerStringValue.push({
        verificationOrder: verificationOrder,
        text: finalString,
        userQuestion: userQuestion.id,
      })

      this.allAnswerStringValue.sort(function (questionA, questionB) {
        return questionA.verificationOrder - questionB.verificationOrder;
      });
    }
  }

  async getUserAnswerFromUserQuestion(userQuestion: UserQuestion) {
    return this.userAnswerRestService.getAllByUserQuestionId(userQuestion.id).toPromise();
  }

  async editAnswer(question: QuestionVersion) {
    var userQuestion: UserQuestion = this.allUserQuestions.find(uq => uq.questionAnswered == question.id);
    var answers: UserAnswer[] = await this.getUserAnswerFromUserQuestion(userQuestion)
    this.router.navigate(['editSurveyAnswer',
      answers[0].id,
      {
        questionVersion: question.id,
        surveyId: this.currentSurvey.id,
        surveyShortName: this.surveyShortName,
        currentUserSurveyId: this.currentUserSurveyId,
        userQuestion: userQuestion,
      }]);
  }

  goBack() {
    this.router.navigate(['']);
  }

}

export interface AnswerStringValue {
  verificationOrder: number,
  text: string,
  userQuestion: string
  supplementInputAnswerString?: string
}
