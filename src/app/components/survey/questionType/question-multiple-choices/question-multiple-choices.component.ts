import { Component, Input, OnInit, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { AnswerChoice } from 'src/app/share/Dto/AnswerChoiceDto';
import { QuestionVersion } from 'src/app/share/Dto/QuestionVersionDto';

@Component({
  selector: 'app-question-multiple-choices',
  templateUrl: './question-multiple-choices.component.html',
  styleUrls: ['./question-multiple-choices.component.scss'],
})
export class QuestionMultipleChoicesComponent implements OnInit {
  completedAnimation = '../../../assets/icons/check-circle.gif';

  @Input() currentQuestion: QuestionVersion;
  @Output() onAnswerChoosed = new EventEmitter<SelectedAnswer>();

  allAnswers: checkboxAnswer[] = [];

  constructor(
  ) { }

  ngOnInit() {
    this.allAnswers = []
    this.initAllAnswer();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.allAnswers = [];
    this.initAllAnswer();
    // this.doSomething(changes.categoryId.currentValue);
    // You can also use categoryId.previousValue and 
    // categoryId.firstChange for comparing old and new values

  }

  initAllAnswer() {
    this.currentQuestion.possibleAnswers.forEach(pa => {
      pa.questionVersion = { id: this.currentQuestion.id };
      this.allAnswers.push({
        val: pa,
        isChecked: false
      })
    });
  }


  async updateCurrentAnswer() {
    var choosedAnswers: AnswerChoice[] = [];
    this.allAnswers.forEach(answer => {
      if (answer.isChecked == true) {
        choosedAnswers.push(answer.val);
      }
    });
    if (choosedAnswers.length == 0) {
      this.onAnswerChoosed.emit({ answers: choosedAnswers, disableNextButton: true });
    } else {
      this.onAnswerChoosed.emit({
        answers: choosedAnswers,
        disableNextButton: false,
      });
    }
  }

  checkTheAnswer(answer: checkboxAnswer) {
    if (answer.isChecked) {
      answer.isChecked = false;
    } else {
      answer.isChecked = true;
    }
  }
}

export interface SelectedAnswer {
  answers: AnswerChoice[];
  disableNextButton: boolean
}

export interface checkboxAnswer {
  val: AnswerChoice;
  isChecked: boolean;
}