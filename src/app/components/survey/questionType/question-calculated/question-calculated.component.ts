import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AnswerChoice } from 'src/app/share/Dto/AnswerChoiceDto';
import { QuestionVersion } from 'src/app/share/Dto/QuestionVersionDto';

@Component({
  selector: 'app-question-calculated',
  templateUrl: './question-calculated.component.html',
  styleUrls: ['./question-calculated.component.scss'],
})
export class QuestionCalculatedComponent implements OnInit {
  completedAnimation = '../../../assets/icons/check-circle.gif';

  @Input() currentQuestion: QuestionVersion;
  @Output() onAnswerChoosed = new EventEmitter<SelectedAnswer>();

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      const newAnswer = { name: "", questionVersion: { id: this.currentQuestion.id } }
      this.onAnswerChoosed.emit({ answers: [newAnswer], disableNextButton: false });
    }, 1000);
  }
}

export interface SelectedAnswer {
  answers: AnswerChoice[];
  disableNextButton: boolean
}
