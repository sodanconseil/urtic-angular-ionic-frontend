import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AnswerChoice } from 'src/app/share/Dto/AnswerChoiceDto';
import { QuestionVersion } from 'src/app/share/Dto/QuestionVersionDto';

@Component({
  selector: 'app-question-string',
  templateUrl: './question-string.component.html',
  styleUrls: ['./question-string.component.scss'],
})
export class QuestionStringComponent implements OnInit {

  @Input() currentQuestion: QuestionVersion;
  @Output() onAnswerChoosed = new EventEmitter<SelectedAnswer>();

  currentAnswer: AnswerChoice;

  constructor() { }

  ngOnInit() { }


  async updateCurrentAnswer(choosedAnswer: AnswerChoice) {
    this.currentAnswer = choosedAnswer;
    choosedAnswer.questionVersion = { id: this.currentQuestion.id };
    choosedAnswer.text = choosedAnswer.supplementInputAnswer;
    this.onAnswerChoosed.emit({ answers: [choosedAnswer], disableNextButton: false });
  }
}

export interface SelectedAnswer {
  answers: AnswerChoice[];
  disableNextButton: boolean
}