import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { AnswerChoice } from 'src/app/share/Dto/AnswerChoiceDto';
import { QuestionVersion } from 'src/app/share/Dto/QuestionVersionDto';

@Component({
  selector: 'app-question-single-choice',
  templateUrl: './question-single-choice.component.html',
  styleUrls: ['./question-single-choice.component.scss'],
})
export class QuestionSingleChoiceComponent implements OnInit {
  completedAnimation = '../../../assets/icons/check-circle.gif';

  @Input() currentQuestion: QuestionVersion;
  @Output() onAnswerChoosed = new EventEmitter<SelectedAnswer>();

  currentAnswer: AnswerChoice;

  constructor() { }

  ngOnInit() {
  }

  updateCurrentAnswer(choosedAnswer: AnswerChoice) {
    this.currentAnswer = choosedAnswer;
    choosedAnswer.questionVersion = { id: this.currentQuestion.id }
    this.onAnswerChoosed.emit({ answers: [choosedAnswer], disableNextButton: false });
  }
}

export interface SelectedAnswer {
  answers: AnswerChoice[];
  disableNextButton: boolean
}
