import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CalendarComponentOptions, DayConfig } from 'ion2-calendar'
import { UserDto } from 'src/app/share/Dto/UserDto';
import { UserSurvey } from 'src/app/share/Dto/UserSurveyDto';
import { UserRestService } from 'src/app/share/user-rest.service';
import { UserSurveyRestService } from 'src/app/share/user-survey-rest.service';

//Tranduction
import { registerLocaleData } from '@angular/common';
import localeEn from '@angular/common/locales/en';
import localeFr from '@angular/common/locales/fr';
import { TranslateService } from '@ngx-translate/core';

//

@Component({
  selector: 'app-survey-calendar',
  templateUrl: './survey-calendar.component.html',
  styleUrls: ['./survey-calendar.component.scss'],
})
export class SurveyCalendarComponent implements OnInit {

  previousIcon = '../../../assets/icons/previous.png';
  nextIcon = '../../../assets/icons/next.png'

  expandLegend: boolean = false;

  displlayCalendar = false;
  currentUser: UserDto = { id: "" };
  surveyShortName: string = "";
  daysConfig: DayConfig[] = [];
  maxDate: string = new Date().toISOString();
  allUserSurvey: UserSurvey[] = [];
  dateRange: {
    from: Date;
    to: Date
  } = {
      from: new Date(new Date().getTime() - (336 * 60 * 60 * 1000)),
      to: new Date()
    };
  options: CalendarComponentOptions = { from: this.dateRange.from, to: this.dateRange.to }


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userRestService: UserRestService,
    private userSurveyRestService: UserSurveyRestService,
    private translateService: TranslateService
  ) { }


  ngOnInit() {
    this.surveyShortName = this.route.snapshot.paramMap.get("surveyShortName");
    this.getUserAndSurvey();
  }

  getUserAndSurvey() {
    this.userRestService.getMe().subscribe((usr: UserDto) => {
      if (usr) {
        if (new Date() < new Date(usr.creationDate)) {
          this.router.navigate(['errorPage'])
        } else {
          this.currentUser = usr;
          this.getAllUserSurvey(usr);
          this.displlayCalendar = true
        }
      }
    });
  }

  ionViewDidEnter() {
    if (this.currentUser.id != '' && new Date() >= new Date(this.currentUser.creationDate)) {
      this.options = { daysConfig: this.daysConfig, from: this.dateRange.from, to: this.dateRange.to }
      this.getAllUserSurvey(this.currentUser);
    }
  }

  getAllUserSurvey(user: UserDto) {
    if (new Date() < new Date(user.creationDate)) {
      this.router.navigate(['errorPage'])
    } else {
      this.userSurveyRestService.getAllUserSurveyByUserIdAndSurveyName(this.surveyShortName, user.id).subscribe((res: UserSurvey[]) => {
        if (res) {
          this.allUserSurvey = res;
          this.initCalendar();
        }
      })
    }
  }

  initCalendar() {
    this.updateDateRange();
    this.initToday();
    this.initUnfilledDaysBeforeRange();
    this.initUnfilledDaysPastRange();
  }

  updateDateRange() {
    var fromDateRange = new Date(this.dateRange.from);
    var userCreationDate = new Date(this.currentUser.creationDate);

    do {
      fromDateRange.setDate(fromDateRange.getDate() + 1)
      if (fromDateRange.toLocaleDateString('en-US') == userCreationDate.toLocaleDateString('en-US')) {
        this.options = { daysConfig: this.daysConfig, from: new Date(userCreationDate), to: new Date() }
      }
    } while (fromDateRange.toLocaleDateString('en-US') != new Date().toLocaleDateString('en-Us'));
  }

  initToday() {
    var now = new Date();
    var fromUserCreationDate = new Date(this.currentUser.creationDate);
    var userSurvey = this.allUserSurvey.find(us => new Date(us.startDate).toLocaleDateString('en-US') == fromUserCreationDate.toLocaleDateString('en-US'));
    fromUserCreationDate = new Date(fromUserCreationDate.setDate(fromUserCreationDate.getDate() - 1));

    if (fromUserCreationDate.toLocaleDateString('en-US') != now.toLocaleDateString('en-US')) {
      do {
        fromUserCreationDate.setDate(fromUserCreationDate.getDate() + 1)
        var userSurvey = this.allUserSurvey.find(us => new Date(us.startDate).toLocaleDateString('en-US') == fromUserCreationDate.toLocaleDateString('en-US'));
        if (fromUserCreationDate.toLocaleDateString('en-US') == now.toLocaleDateString('en-US')) {
          this.initCompletedAndInProgressDays(fromUserCreationDate, userSurvey ? userSurvey : {}, true)
        }
      } while (fromUserCreationDate.toLocaleDateString('en-US') != now.toLocaleDateString('en-US'));
    } else {
      this.initCompletedAndInProgressDays(fromUserCreationDate, userSurvey ? userSurvey : {})
    }
  }

  iniNewDate(userSurvey: UserSurvey, options: { cssClass?: string }, isDisabled: boolean = false) {
    var toAdd: DayConfig = {
      date: new Date(new Date(userSurvey.startDate).toLocaleDateString('en-US')),
      cssClass: options.cssClass,
      disable: isDisabled
    }
    this.daysConfig.push(toAdd)
  }

  initUnfilledDaysBeforeRange() {
    var fromDate = new Date(this.options.from);

    var fromUserCreationDate = new Date(this.currentUser.creationDate);

    if (fromUserCreationDate.toLocaleDateString('en-US') != fromDate.toLocaleDateString('en-US')) {
      do {
        var userSurvey = this.allUserSurvey.find(us => new Date(us.startDate).toLocaleDateString('en-US') == fromUserCreationDate.toLocaleDateString('en-US'));
        if (userSurvey) {
          this.initCompletedAndInProgressDays(fromUserCreationDate, userSurvey);
        } else {
          this.iniNewDate({ startDate: fromUserCreationDate }, { cssClass: "survey_danger_disabled" }, true)
        }

        fromUserCreationDate.setDate(fromUserCreationDate.getDate() + 1)
      } while (fromUserCreationDate.toLocaleDateString('en-US') != fromDate.toLocaleDateString('en-US'));
    }
  }

  initUnfilledDaysPastRange() {
    var now = new Date();
    var fromDateRange = new Date(this.options.from);
    fromDateRange = new Date(fromDateRange.setDate(fromDateRange.getDate() - 1));

    do {
      fromDateRange.setDate(fromDateRange.getDate() + 1)

      var userSurvey = this.allUserSurvey.find(us => new Date(us.startDate).toLocaleDateString('en-US') == fromDateRange.toLocaleDateString('en-US'));
      if (userSurvey) {
        this.initCompletedAndInProgressDays(fromDateRange, userSurvey);
      } else {
        this.iniNewDate({ startDate: fromDateRange }, { cssClass: "survey_danger" })
      }
    } while (fromDateRange.toLocaleDateString('en-US') != now.toLocaleDateString('en-US'));
  }

  initCompletedAndInProgressDays(fromUserCreationDate: Date, userSurvey: UserSurvey, today: boolean = false) {
    //Completed
    if (userSurvey.dateFilled != null && !today) {
      this.iniNewDate(userSurvey, { cssClass: "survey_completed" }, true)
    }

    //Completed and still editable
    if (userSurvey.dateFilled != null && today) {
      this.iniNewDate(userSurvey, { cssClass: "survey_completed_and_editable" }, false)
    }

    //In progress
    if (userSurvey.startDate != null && userSurvey.dateFilled == null && fromUserCreationDate >= this.options.from) {
      this.iniNewDate(userSurvey, { cssClass: "survey_in_progress" })
    }

    //In progress
    if (userSurvey.startDate != null && userSurvey.dateFilled == null && fromUserCreationDate <= this.options.from) {
      this.iniNewDate(userSurvey, { cssClass: "survey_in_progress" }, true)
    }

    //Today Todo
    if (userSurvey.creationDate == null) {
      this.iniNewDate({ startDate: fromUserCreationDate }, { cssClass: "survey_todo" })
    }
  }

  updateChoosedDate(val) {
    var splitDate = val.split('-')
    var yyyy = splitDate[0]
    var mm = splitDate[1]
    var dd = splitDate[2]

    var choosedDate = new Date(Date.parse(val))
    choosedDate.setDate(choosedDate.getDate() + 1)

    var userSurveyFromChoosedDate = this.allUserSurvey.find(us => new Date(us.startDate).toLocaleDateString('en-US') == choosedDate.toLocaleDateString('en-US'))
    if (this.allUserSurvey == null || this.allUserSurvey.length == 0) {
      this.router.navigate(['survey', { surveyShortName: this.surveyShortName }]);
    }
    if (userSurveyFromChoosedDate == null) {
      this.router.navigate(['survey', { choosedDate: choosedDate, surveyShortName: this.surveyShortName }]);
    }
    if (userSurveyFromChoosedDate != null && userSurveyFromChoosedDate.dateFilled == null) {
      this.router.navigate(['survey', { surveyShortName: this.surveyShortName, choosedDate: choosedDate, userSurveyId: userSurveyFromChoosedDate.id }]);
    }
    if (userSurveyFromChoosedDate != null && userSurveyFromChoosedDate.dateFilled != null) {
      // this.router.navigate(['survey', { surveyShortName: this.surveyShortName, choosedDate: choosedDate, userSurveyId: userSurveyFromChoosedDate.id }]);
      this.router.navigate(['surveyAnswerManager', userSurveyFromChoosedDate.id, { surveyShortName: this.surveyShortName }]);
    }
  }

  expandAndCloseLegend() {
    if (this.expandLegend) {
      this.expandLegend = false;
    } else {
      this.expandLegend = true;
    }
  }

  goBack() {
    this.router.navigate(['']);
  }

}