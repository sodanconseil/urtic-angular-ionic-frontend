import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';
import { DrugRestService } from 'src/app/share/drug-rest.service';
import { DrugDto } from 'src/app/share/Dto/DrugDto';

@Component({
  selector: 'app-med-list-search',
  templateUrl: './med-list-search.component.html',
  styleUrls: ['./med-list-search.component.scss'],
})
export class MedListSearchComponent implements OnInit {

  medBottleIcon = '../../../assets/icons/pills.png';
  deleteIcon = '../../../assets/icons/delete.png';
  plusIcon = '../../../assets/icons/plus.png';
  previousIcon = '../../../assets/icons/previous.png';

  dbMedList: DrugDto[] = [];

  medSearch = new FormControl('');
  medicamentsList: DrugDto[] = [];

  constructor(
    private router: Router,
    private drugRestService: DrugRestService
  ) { }

  ngOnInit(): void {
    this.getItems()
  }

  ionViewWillEnter() {
    this.getItems()
  }

  getItems() {
    this.drugRestService.getAll().subscribe((res) => {
      res.forEach(med => {
        med.name = med.name.charAt(0).toUpperCase() + med.name.slice(1);
      });
      this.dbMedList = res;
      this.medicamentsList = res;
    })
  }


  getSearchResult(inputValue: any) {
    let medSearch = inputValue.target.value.toLowerCase();
    this.medicamentsList = [];
    if (medSearch.lenght) {
      this.medicamentsList = this.dbMedList;
    }
    this.medicamentsList = this.dbMedList.filter(m =>
      (m.name != null && m.name.toLowerCase().includes(medSearch)) ||
      (m.commercial_name != null && m.commercial_name.toLowerCase().includes(medSearch))
    )
  }

  delSearch() {
    this.medSearch.setValue('');
    this.medicamentsList = [];
  }

  goToMedInfo(med: DrugDto) {
    this.router.navigate(['medInformation'], {
      state: {
        medicament: med,
        prescription: ''
      }
    });
  }

  goToMedManager() {
    this.router.navigate(['medManager']);
  }

}

export interface Medicament {
  genName: string;
  commName: string;
  dosage: number;
  marqueGen: string[];
  frequence?: MedFrequence;
  quantite?: number;
}

export interface MedFrequence {
  numberOfTime: number;
  perTime: number;
}
