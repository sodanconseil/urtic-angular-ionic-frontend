import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedListSearchComponent } from './med-list-search.component';

describe('MedListSearchComponent', () => {
  let component: MedListSearchComponent;
  let fixture: ComponentFixture<MedListSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedListSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedListSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
