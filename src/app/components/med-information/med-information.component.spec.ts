import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedInformationComponent } from './med-information.component';

describe('MedInformationComponent', () => {
  let component: MedInformationComponent;
  let fixture: ComponentFixture<MedInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
