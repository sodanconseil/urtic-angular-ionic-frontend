/* eslint-disable eqeqeq */
/* eslint-disable @typescript-eslint/prefer-for-of */
/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable no-var */
/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/semi */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DosageDto } from 'src/app/share/Dto/DosageDto';
import { DrugDto } from 'src/app/share/Dto/DrugDto';
import { PrescriptionDto } from 'src/app/share/Dto/PrescriptionDto';
import { PrescriptionRestService } from 'src/app/share/prescription-rest.service';
import { Medicament } from '../med-list-search/med-list-search.component';
import { FrequencyDto } from 'src/app/share/Dto/FrequencyDto';
import { FrequencyRestService } from 'src/app/share/frequency-rest.service';
import { UserRestService } from 'src/app/share/user-rest.service';
import { DosageRestService } from 'src/app/share/dosage-rest.service';
import { DrugRestService } from 'src/app/share/drug-rest.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-med-information',
  templateUrl: './med-information.component.html',
  styleUrls: ['./med-information.component.scss']
})
export class MedInformationComponent implements OnInit {

  language: string = this.translateService.currentLang;

  choosedMed: DrugDto = null;
  currentPrescription: string = null;

  medBottleIcon = '../../../assets/icons/pills.png';
  deleteIcon = '../../../assets/icons/delete.png';
  plusIcon = '../../../assets/icons/plus.png';
  previousIcon = '../../../assets/icons/previous.png';
  nextIcon = '../../../assets/icons/next.png'

  allDosage: DosageDto[] = [];
  allFrequency: FrequencyDto[] = [];
  userMedList: DrugDto[] = [];

  inputDosage;
  inputQuantity;
  inputFrequency;
  inputPerTime;

  currentUser: any;

  dbMedList: Medicament[] = [];

  constructor(
    private router: Router,
    private prescriptionRestService: PrescriptionRestService,
    private frequenceRestService: FrequencyRestService,
    private userRestService: UserRestService,
    private dosageRestService: DosageRestService,
    private frequencyRestService: FrequencyRestService,
    private drugRestService: DrugRestService,
    private translateService: TranslateService
  ) { }

  ngOnInit(): void {
    this.getItems();
  }

  ionViewWillEnter() {
    if (this.choosedMed == null) {
      this.getItems()
    }
  }

  getItems() {
    this.choosedMed = history.state.medicament;
    this.currentPrescription = history.state.prescription

    //this.currentUser = JSON.parse(window.localStorage.getItem('currentUser'));

    this.userRestService.getMe().subscribe((usr) => {

      this.currentUser = usr;

      this.choosedMed.dosages.forEach(dosage => {
        this.allDosage.push(dosage)
      });

      this.frequenceRestService.getAllFrequences().subscribe((res: FrequencyDto[]) => {
        if (res) {

          for (let index = 0; index < res.length; index++) {
            const frequency = res[index];


            if (this.translateService.currentLang == "en") {
              frequency.name = frequency.en;
            } if (this.translateService.currentLang == "fr") {
              frequency.name = frequency.fr;
            }
          }
          this.allFrequency = res
          var uniqueIndex = this.allFrequency.indexOf(this.allFrequency.find(f => f.annualized_value == 0));
          this.allFrequency.splice(uniqueIndex, 1);


          this.allFrequency.sort((frequency1, frequency2) => frequency1.annualized_value - frequency2.annualized_value);
          if (this.currentPrescription === "") {

            this.inputDosage = this.allDosage[0].id;
            this.inputQuantity = 1;
            this.inputFrequency = 1;
            this.inputPerTime = this.allFrequency[0].id;

          } else {

            const fakeThis = this;

            this.userRestService.getAllMePrescription().subscribe((res) => {
              const currentPrescription = res.filter(res => res.id === fakeThis.currentPrescription)[0];
              fakeThis.dosageRestService.get(currentPrescription.dosageId).subscribe((res) => fakeThis.inputDosage = res.id);
              fakeThis.inputQuantity = currentPrescription.quantity;
              fakeThis.inputFrequency = currentPrescription.frequencyQuantity;
              fakeThis.frequenceRestService.get(currentPrescription.frequencyId).subscribe(res => fakeThis.inputPerTime = res.id);
              fakeThis.inputPerTime = currentPrescription.frequencyQuantity;
            })
          }
        }
      }), (err) => {
        console.log(err);
      };
    })
  }

  toggleQuantityInformation() {
    this.translateService.get('MedInformation.UnitHelpMessage').subscribe((res) => {
      alert(res);
    })
  }

  goBack() {
    window.history.back();
  }

  save() {
    let max = 999999;
    var newPrescription: PrescriptionDto = {
      name: `${this.choosedMed.name} ${this.choosedMed.commercial_name == null ? '' : '(' + this.choosedMed.commercial_name + ')'}`,
      drugId: this.choosedMed.id,
      userId: this.currentUser.id,
      frequencyId: this.allFrequency.find(f => f.id == this.inputPerTime).id,
      dosageId: this.allDosage.find(d => d.id = this.inputDosage).id,
      quantity: this.inputQuantity >= 999999 ? 99999 : this.inputQuantity,

      frequencyQuantity: this.inputFrequency
    };

    if (this.currentPrescription == (null || "")) {
      this.prescriptionRestService.createPrescription(newPrescription).subscribe((res) => {
        if (res) {
          this.router.navigate(['medManager'])
        }
      }), (err) => {
        console.log(err)
      }
    } else {
      newPrescription.etag = '1';
      this.prescriptionRestService.editPrescription(this.currentPrescription, newPrescription).subscribe((res) => {
        if (res) {
          this.router.navigate(['medManager'])
        }
      }), (err) => {
        console.log(err)
      }
    }

  }
}
