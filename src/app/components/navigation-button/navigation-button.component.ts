import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-navigation-button',
  templateUrl: './navigation-button.component.html',
  styleUrls: ['./navigation-button.component.scss'],
})
export class NavigationButtonComponent implements OnInit {

  // Hidden
  @Input() hiddenLeftButton?: boolean;
  @Input() hiddenRightButton?: boolean;

  // //Action*
  // @Input() leftButtonFunction: Function;
  // @Input() rightButtonFunction?: Function;

  //Disabled*
  @Input() disableLeftButton: boolean;
  @Input() disableRightButton: boolean;

  // //Icon*
  @Input() leftButtonIcon?: string;
  @Input() rightButtonIcon?: string;

  //Text*
  @Input() leftButtonText?: string;
  @Input() rightButtonText?: string;

  //Click
  @Output() onLeftButtonClicked?= new EventEmitter<boolean>();
  @Output() onRightButtonClicked?= new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {

  }

  clickLeftButton() {
    this.onLeftButtonClicked.emit(true);
  }

  clickRightButton() {
    this.onRightButtonClicked.emit(true);
  }

}
