import { Component, OnInit } from '@angular/core';

//Translation
import { TranslateService } from '@ngx-translate/core';
//

//Translation
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ActivatedRoute, Router } from '@angular/router';
import { UserDto } from 'src/app/share/Dto/UserDto';
import { UserRestService } from 'src/app/share/user-rest.service';
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}
//

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {

  previousIcon = '../../../assets/icons/previous.png'
  language: string
  allLangs = [];

  currentUser: UserDto = null;

  newLanguage: string;

  constructor(
    private router: Router,
    private translateService: TranslateService,
    private userRestService: UserRestService,
    private route: ActivatedRoute
  ) { }

  async ngOnInit() {
    await this.getItems();
  }

  async getItems() {
    // await this.getRouterParams(); 
    this.allLangs = this.translateService.getLangs();
    this.currentUser = await this.getUser();
    this.language = this.currentUser.prefferedLanguage
    this.newLanguage = this.currentUser.prefferedLanguage;
  }

  async ionViewWillEnter() {
    this.currentUser = await this.getUser();
    this.language = this.currentUser.prefferedLanguage;
    this.newLanguage = this.currentUser.prefferedLanguage;
  }

  async getUser() {
    return this.userRestService.getMe().toPromise();
  }


  // async getRouterParams() {
  //   return new Promise((resolve, reject) => {

  //     resolve(null);
  //   });
  // }

  async editUser() {
    var newUser: UserDto = JSON.parse(JSON.stringify(this.currentUser));
    newUser.prefferedLanguage = this.newLanguage;
    return this.userRestService.editUser(newUser, this.currentUser.id).toPromise();
  }


  //Translation
  async languageChange() {
    await this.editUser();
    this.translateService.use(this.newLanguage);
  }
  //

  goToAccount() {
    this.router.navigate([''])
  }

}
