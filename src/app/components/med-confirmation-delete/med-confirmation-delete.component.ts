import { Component, OnInit } from '@angular/core';
import { PrescriptionRestService } from 'src/app/share/prescription-rest.service';
import { ActivatedRoute, NavigationEnd } from '@angular/router';
import { Router } from '@angular/router';
import { DrugDto } from 'src/app/share/Dto/DrugDto';
import { PrescriptionDto } from 'src/app/share/Dto/PrescriptionDto';
import { DosageRestService } from 'src/app/share/dosage-rest.service';
import { DosageDto } from 'src/app/share/Dto/DosageDto';
import { FrequencyRestService } from 'src/app/share/frequency-rest.service';

@Component({
  selector: 'app-med-confirmation-delete',
  templateUrl: './med-confirmation-delete.component.html',
  styleUrls: ['./med-confirmation-delete.component.scss'],
})
export class MedConfirmationDeleteComponent implements OnInit {
  nextIcon = '../../../assets/icons/next.png';
  previousIcon = '../../../assets/icons/previous.png';
  warningIcon = '../../../assets/icons/warning.png'
  medBottleIcon = '../../../assets/icons/pills.png'

  choosedMed: PrescriptionDto = null;
  currentFrequency;
  medDosage;

  constructor(
    private prescriptionRestService: PrescriptionRestService,
    public activatedRoute: ActivatedRoute,
    private router: Router,
    private dosageRestService: DosageRestService,
    private frequencyRestService: FrequencyRestService
  ) { }

  ngOnInit() {
    this.choosedMed = history.state.medicament;
    this.getDosage();
    this.getFrequency();
  }

  getFrequency() {
    this.frequencyRestService.getFrequencyById(this.choosedMed.frequencyId).subscribe((res) => {
      this.currentFrequency = res.name;
    })
  }

  getDosage() {
    this.dosageRestService.getDosageById(this.choosedMed.dosageId).subscribe((res: DosageDto) => {
      this.medDosage = res.description
    })
  }

  goBack() {
    window.history.back();
  }

  delete() {
    this.prescriptionRestService.deletePrescription(this.choosedMed.id).subscribe(res => {
      if (res) {
        this.router.navigate(['medManager']);
      }
    }), (err) => {
      console.log(err)
    }
  }

}
