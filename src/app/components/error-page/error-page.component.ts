import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.scss'],
})
export class ErrorPageComponent implements OnInit {
  error_animated = '../../../assets/icons/error_animated.gif';

  constructor(
    private router: Router
  ) { }

  ngOnInit() { }

  gotToHome() {
    this.router.navigate(['']);
  }
}
