import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-picture',
  templateUrl: './picture.component.html',
  styleUrls: ['./picture.component.scss'],
})
export class PictureComponent implements OnInit {

  previousIcon = '../../../assets/icons/previous.png';
  nextIcon = '../../../assets/icons/next.png'
  pictureImage = "../../../assets/icons/image.png";

  allPics: String[] = []

  constructor() { }


  ngOnInit() {
  }

  addPicture() {
    this.allPics.push("");
  }

  goBack() {
    window.history.back();
  }


}
