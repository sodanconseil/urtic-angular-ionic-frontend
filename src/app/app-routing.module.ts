import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { MedManagerComponent } from './components/med-manager/med-manager.component';
import { MedListSearchComponent } from './components/med-list-search/med-list-search.component';
import { MedInformationComponent } from './components/med-information/med-information.component';
import { CompletProfileFormComponent } from './components/complete-profile-form/complete-profile-form.component';
import { MedConfirmationDeleteComponent } from './components/med-confirmation-delete/med-confirmation-delete.component';
import { SurveyComponent } from './components/survey/survey.component';
import { SurveyCalendarComponent } from './components/survey/calendar/survey-calendar.component';
import { ErrorPageComponent } from './components/error-page/error-page.component';
// import { SurveyGroupComponent } from './components/home/survey-group/survey-group.component';
import { SurveyAnswerManagerComponent } from './components/survey/edit survey/survey-answer-manager/survey-answer-manager.component';
import { EditSurveyAnswerComponent } from './components/survey/edit survey/edit-survey-answer/edit-survey-answer.component';
import { ProfileComponent } from './components/profile/profile.component';
import { PictureComponent } from './components/picture/picture.component';


const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./components/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  { path: 'medManager', component: MedManagerComponent },
  { path: 'medListSearch', component: MedListSearchComponent },
  { path: 'medInformation', component: MedInformationComponent },
  { path: 'completeProfile', component: CompletProfileFormComponent },
  { path: 'medDeleteInformation', component: MedConfirmationDeleteComponent },
  { path: 'survey', component: SurveyComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'picture', component: PictureComponent },
  { path: 'surveyCalendar/:surveyShortName', component: SurveyCalendarComponent },
  { path: 'errorPage', component: ErrorPageComponent },
  // { path: 'surveyGroup/:surveyShortName', component: SurveyGroupComponent },
  { path: 'surveyAnswerManager/:userSurveyId', component: SurveyAnswerManagerComponent },
  { path: 'editSurveyAnswer/:choosedAnswerId', component: EditSurveyAnswerComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
