import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { BaseRestService } from './rest.service'

@Injectable({
  providedIn: 'root'
})
export class FrequencyRestService extends BaseRestService {

  URL_POC_TAG = 'frequences';

  getContext(): string {
    return this.URL_POC_TAG;
  }

  constructor(public configService: ConfigService, public http: HttpClient) {
    super(configService, http);
  }

  public getAllFrequences() {
    return this.httpGet(this.configService.environmentConfig.getApiServer() + this.getContext());
  }
  public getFrequencyById(id: string, callback?: Function) {
    return this.httpGet(this.configService.environmentConfig.getApiServer() + this.getContext() + '/' + id, callback);
  }
}
