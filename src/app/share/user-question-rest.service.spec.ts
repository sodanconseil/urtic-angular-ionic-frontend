import { TestBed } from '@angular/core/testing';

import { UserQuestionRestService } from './user-question-rest.service';

describe('UserQuestionRestService', () => {
  let service: UserQuestionRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserQuestionRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
