import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ConfigService } from './config.service';
import { BaseRestService } from './rest.service'
import { TranslateService } from '@ngx-translate/core';


@Injectable({
  providedIn: 'root'
})
export class QuestionVersionRestService extends BaseRestService {

  URL_POC_TAG = 'questionversion';

  getContext(): string {
    return this.URL_POC_TAG;
  }

  constructor(public configService: ConfigService, public http: HttpClient, public translateService: TranslateService) {
    super(configService, http);
  }

  public getById(questionVersionId: string, callback?: Function) {
    return this.httpGet(this.configService.environmentConfig.getApiServer() + this.getContext() + "/" + this.translateService.currentLang + "/" + questionVersionId, callback);
  }
}
