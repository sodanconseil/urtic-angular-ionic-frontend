import { TestBed } from '@angular/core/testing';

import { DrugRestService } from './drug-rest.service';

describe('DrugRestService', () => {
  let service: DrugRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DrugRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
