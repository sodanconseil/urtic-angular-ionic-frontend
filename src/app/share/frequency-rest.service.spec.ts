import { TestBed } from '@angular/core/testing';

import { FrequencyRestService } from './frequency-rest.service';

describe('FrequencyRestService', () => {
  let service: FrequencyRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FrequencyRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
