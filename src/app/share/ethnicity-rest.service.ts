import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { BaseRestService } from './rest.service'

@Injectable({
  providedIn: 'root'
})
export class EthnicityRestService extends BaseRestService {

  URL_POC_TAG = 'ethnicities';

  getContext(): string {
    return this.URL_POC_TAG;
  }

  constructor(public configService: ConfigService, public http: HttpClient) {
    super(configService, http);
  }
}
