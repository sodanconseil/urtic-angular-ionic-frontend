import { TestBed } from '@angular/core/testing';

import { SubQuestionConditionRestService } from './sub-question-condition-rest.service';

describe('SubQuestionConditionRestService', () => {
  let service: SubQuestionConditionRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubQuestionConditionRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
