import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ConfigService } from './config.service';
import { BaseRestService } from './rest.service'


@Injectable({
  providedIn: 'root'
})
export class UserAnswerRestService extends BaseRestService {

  URL_POC_TAG = 'useranswer';

  getContext(): string {
    return this.URL_POC_TAG;
  }

  constructor(public configService: ConfigService, public http: HttpClient) {
    super(configService, http);
  }

  public edit(id: string, model: any) {
    return this.httpPut(this.configService.environmentConfig.getApiServer() + this.getContext() + '/' + id, model);
  }

  public getAllByUserQuestionId(userQuestionId: string) {
    return this.httpGet(this.configService.environmentConfig.getApiServer() + this.getContext() + '/all/' + userQuestionId);
  }
}
