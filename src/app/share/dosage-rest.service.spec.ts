import { TestBed } from '@angular/core/testing';

import { DosageRestService } from './dosage-rest.service';

describe('DosageRestService', () => {
  let service: DosageRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DosageRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
