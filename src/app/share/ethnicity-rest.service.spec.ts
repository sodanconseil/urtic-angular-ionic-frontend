import { TestBed } from '@angular/core/testing';

import { EthnicityRestService } from './ethnicity-rest.service';

describe('EthnicityRestService', () => {
  let service: EthnicityRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EthnicityRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
