import { FrequencyDto } from "./FrequencyDto";
import { QuestionVersion } from "./QuestionVersionDto";
import { Survey } from "./SurveyDto";

export interface SurveyVersion {
    id?: string;
    name?: string;
    shortName?: string;
    description?: string;
    creationUser?: string;
    creationDate?: Date;
    modificationUser?: string;
    modificationDate?: Date;
    survey?: Survey;
    survey_version?: number;
    date?: Date;
    questionVersions?: QuestionVersion[];
    frequency?: FrequencyDto;
    delais?: string;
    en?: string
    fr?: string
}