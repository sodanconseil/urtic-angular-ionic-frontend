import { AnswerChoice } from "./AnswerChoiceDto";
import { UserQuestion } from "./UserQuestionDto";

export interface UserAnswer {
    version?: number;
    id?: string;
    name?: string;
    description?: string;
    creationUser?: string;
    creationDate?: Date;
    modificationUser?: string;
    modificationDate?: Date;
    answerChoice_id: string;
    intAnswer?: number;
    intMinAnswer?: number;
    intMaxAnswer?: number;
    floatAnswer?: number;
    floatMinAnswer?: number;
    floatMaxAnswer?: number;
    stringAnswer?: string;
    dateAnswer?: Date;
    startDateAnswer?: Date;
    endDateAnswer?: Date;
    userQuestion?: UserQuestion;
    supplementInputAnswer?: string

    //Response
    subConditionIsMet?: boolean;
}