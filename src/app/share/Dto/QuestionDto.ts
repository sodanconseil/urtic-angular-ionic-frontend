import { Timestamp } from "rxjs/internal/operators/timestamp";

export interface Question {
    id?: string;
    version?: number;
    name?: string;
    description?: string;
    creationUser?: string;
    creationDate?: Date;
    modificationUser?: string;
    modificationDate?: Date;
    text?: string;
}