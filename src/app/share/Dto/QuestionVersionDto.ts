import { AnswerChoice } from "./AnswerChoiceDto";
import { Question } from "./QuestionDto";
import { QuestionType } from "./QuestionTypeDto";

export interface QuestionVersion {
    id?: string;
    version?: number;
    name?: string;
    description?: string;
    creationUser?: string;
    creationDate?: Date;
    modificationUser?: string;
    modificationDate?: Date;
    question_order?: number;
    question?: Question;
    questionType?: QuestionType;
    text?: string;
    formattedText?: string;
    info?: string;
    formattedInfo?: string;
    surveyVersion?: any;
    possibleAnswers?: AnswerChoice[];
    commandClass?: string;
    value?: number;
    hidden?: boolean;
    noAnswerEndSurvey?: boolean;
    subQuestion?: boolean;
    subQuestionOrder?: number;
    verificationOrder?: number;

}