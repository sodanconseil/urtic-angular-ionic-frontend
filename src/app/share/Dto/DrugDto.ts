import { DosageDto } from "./DosageDto";

export interface DrugDto {
    id: string;
    version: number;
    name: string;
    description: string;
    creationUser: string;
    creationDate: Date;
    modificationUser: string;
    modificationDate: Date;
    commercial_name: string;
    dosages: DosageDto[]
}