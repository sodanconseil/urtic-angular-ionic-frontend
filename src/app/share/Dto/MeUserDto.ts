import { PrescriptionDto } from "./PrescriptionDto";

export interface MeUserDto {
    id?: string;
    name?: string;
    description?: string;
    creationUser?: string;
    creationDate?: Date;
    modificationUser?: string;
    modificationDate?: Date;
    firstName?: string;
    middleName?: string;
    email?: string;
    birthday?: Date;
    phoneNumber?: string;
    gender?: string;
    type?: string;
    emailValid?: boolean;
    phoneNumberValid?: boolean;
    lastAuthentication?: Date;
    prescriptions?: PrescriptionDto[];
}