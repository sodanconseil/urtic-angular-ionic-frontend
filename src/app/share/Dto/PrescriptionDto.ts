import { DosageDto } from "./DosageDto";
import { DrugDto } from "./DrugDto";
import { FrequencyDto } from "./FrequencyDto";
import { UserDto } from "./UserDto";

export interface PrescriptionDto {
    id?: string;
    version?: number;
    name: string;
    description?: string;
    creationUser?: string;
    creationDate?: Date;
    modificationUser?: string;
    modificationDate?: Date;
    drugId?: string;
    userId?: string;
    frequencyId?: string;
    dosageId?: string;
    quantity?: number;
    frequencyQuantity?: number;
    etag?: string;
}