import { QuestionVersion } from "./QuestionVersionDto";
import { UserAnswer } from "./UserAnswerDto";

export interface UserQuestion {
    id?: string;
    name?: string
    questionAnswered?: QuestionVersion;
    answers?: UserAnswer[];
    dateAnswered?: Date;
    description?: string;
    creationUser?: string;
    creationDate?: Date;
    modificationUser?: string;
    modificationDate?: Date;
    hasCompletedSubQuestionCondition?: boolean;
    user_survey_Id?: string;
}