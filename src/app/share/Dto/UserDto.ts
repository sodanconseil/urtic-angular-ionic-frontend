import { EthnicityDto } from "./EthnicityDto";

export interface UserDto {
    id?: string;
    version?: number;
    name?: string;
    description?: string;
    creationUser?: string;
    creationDate?: Date;
    modificationUser?: string;
    modificationDate?: Date;
    keycloakId?: string;
    firstName?: string;
    middleName?: string;
    email?: string;
    birthday?: Date;
    phoneNumber?: string;
    gender?: string;
    type?: string;
    emailValid?: boolean;
    phoneNumberValid?: boolean;
    lastAuthentication?: Date;
    ethnicities?: EthnicityDto[];
    prefferedLanguage?: string;
}