import { Timestamp } from "rxjs/internal/operators/timestamp";
import { Survey } from "./SurveyDto";
import { SurveyVersion } from "./SurveyVersionDto";
import { UserDto } from "./UserDto";
import { UserQuestion } from "./UserQuestionDto";

export interface UserSurvey {
    version?: number;
    id?: string;
    name?: string;
    description?: string;
    creationUser?: string;
    creationDate?: Date;
    modificationUser?: string;
    modificationDate?: Date;
    survey_version_Id?: string;
    user_id?: string;
    answeredQuestions?: UserQuestion[];
    dateFilled?: any;
    startDate?: any;
}