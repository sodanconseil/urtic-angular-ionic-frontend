import { AnswerChoice } from "./AnswerChoiceDto";

export interface SubQuestionCondition {
    id?: string,
    name?: string,
    description?: string,
    creationUser?: string,
    creationDate?: Date,
    modificationUser?: string,
    modificationDate?: Date,
    questionVersion?: string,
    answerChoices?: AnswerChoice[],
    surveyVersion?: string,
    subQuestionVersion?: string
    subQuestionOrder?: number,
}