import { QuestionVersion } from "./QuestionVersionDto";

export interface AnswerChoice {
    version?: number;
    id?: string;
    name?: string;
    description?: string;
    creationUser?: string;
    creationDate?: Date;
    modificationUser?: string;
    modificationDate?: Date;
    code?: number;
    answer_order?: number;
    value?: number;
    text?: string;
    formattedText?: string;
    info?: string;
    formattedInfo?: string;
    questionVersion?: QuestionVersion;
    supplementInput?: boolean;
    supplementInputText?: string;
    subQuestionCondition?: string;
    endSurvey?: boolean;

    //Only request
    supplementInputAnswer?: string;
}