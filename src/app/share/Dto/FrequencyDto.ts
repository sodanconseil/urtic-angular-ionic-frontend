export class FrequencyDto {
    id?: string;
    version?: number;
    name?: string;
    description?: string;
    creationUser?: string;
    creationDate?: Date;
    modificationUser?: string;
    modificationDate?: Date;
    annualized_value?: number;
    fr?: string;
    en?: string;

    public toString(): string {
        return this.annualized_value + "";
    }
}