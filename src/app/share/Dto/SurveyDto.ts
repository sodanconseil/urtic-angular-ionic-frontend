import { Timestamp } from "rxjs/internal/operators/timestamp";
import { Question } from "./QuestionDto";

export interface Survey {
    id: string;
    name: string;
    description: string;
    creationUser: string;
    creationDate: Date;
    modificationUser: string;
    modificationDate: Date;
    shortName: string;
    questions: Question[];
}