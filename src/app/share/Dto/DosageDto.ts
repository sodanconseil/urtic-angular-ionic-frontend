export interface DosageDto {
    id: string;
    version: number;
    name: string;
    description: string;
    creationUser: string;
    creationDate: Date;
    modificationUser: string;
    modificationDate: Date;
}