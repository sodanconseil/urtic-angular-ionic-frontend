import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { BaseRestService } from './rest.service';

@Injectable({
  providedIn: 'root'
})
export class SubQuestionConditionRestService extends BaseRestService {
  URL_POC_TAG = 'subquestioncondition';

  getContext(): string {
    return this.URL_POC_TAG;
  }

  constructor(public configService: ConfigService, public http: HttpClient) {
    super(configService, http);
  }

  public getAllBySurveyVersion(surveyVersionId: string, callback?: Function) {
    return this.httpGet(this.configService.environmentConfig.getApiServer() + this.getContext() + '/' + surveyVersionId, callback);
  }
}
