import { TestBed } from '@angular/core/testing';

import { UserSurveyRestService } from './user-survey-rest.service';

describe('UserSurveyRestService', () => {
  let service: UserSurveyRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserSurveyRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
