import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { AnswerChoice } from './Dto/AnswerChoiceDto';
import { UserSurvey } from './Dto/UserSurveyDto';
import { BaseRestService } from './rest.service';

@Injectable({
  providedIn: 'root'
})
export class UserSurveyRestService extends BaseRestService {

  URL_POC_TAG = 'usersurvey';

  getContext(): string {
    return this.URL_POC_TAG;
  }

  constructor(public configService: ConfigService, public http: HttpClient) {
    super(configService, http);
  }

  public postUserSurvey(model: any, callback?: Function) {
    return this.httpPost(this.configService.environmentConfig.getApiServer() + this.getContext(), model, callback);
  }

  public postUserAnswer(id: String, model: AnswerChoice[]) {
    return this.httpPost(this.configService.environmentConfig.getApiServer() + this.getContext() + "/" + id + "/questions", model);
  }

  public getAllUserSurveyByUserIdAndSurveyName(surveyShortName: string, userId: string, callback?: Function) {
    return this.httpGet(this.configService.environmentConfig.getApiServer() + this.getContext() + "/allForUser/" + surveyShortName + "/" + userId, callback);
  }

  public editUserSurvey(id: string, model: UserSurvey, callback?: Function) {
    return this.httpPut(this.configService.environmentConfig.getApiServer() + this.getContext() + '/' + id, model, callback);
  }

}

