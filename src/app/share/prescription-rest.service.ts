import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ConfigService } from './config.service';
import { BaseRestService } from './rest.service'

@Injectable({
  providedIn: 'root'
})
export class PrescriptionRestService extends BaseRestService {

  URL_POC_TAG = 'prescriptions';

  getContext(): string {
    return this.URL_POC_TAG;
  }

  constructor(public configService: ConfigService, public http: HttpClient) {
    super(configService, http);
  }
  // CREATE
  public createPrescription(model: any, callback?: Function) {
    return this.httpPost(this.configService.environmentConfig.getApiServer() + this.getContext(), model);
  }

  // EDIT
  // tslint:disable-next-line:ban-types
  public editPrescription(id: string, model: any, callback?: Function) {
    return this.httpPut(this.configService.environmentConfig.getApiServer() + this.getContext() + '/' + id, model);
  }

  // DELETE
  // tslint:disable-next-line:ban-types
  public deletePrescription(id: string, callback?: Function) {
    return this.httpDelete(this.configService.environmentConfig.getApiServer() + this.getContext() + '/' + id);
  }

}


