import { TestBed } from '@angular/core/testing';

import { QuestionVersionRestService } from './question-version-rest.service';

describe('QuestionVersionRestService', () => {
  let service: QuestionVersionRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuestionVersionRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
