import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ConfigService } from './config.service';
import { BaseRestService } from './rest.service';


@Injectable({
  providedIn: 'root'
})
export class SurveyRestService extends BaseRestService {

  URL_POC_TAG = 'survey';

  getContext(): string {
    return this.URL_POC_TAG;
  }

  constructor(public configService: ConfigService, public http: HttpClient, private translateService: TranslateService) {
    super(configService, http);
  }

  public getAll(callback?: Function) {
    return this.httpGet(this.configService.environmentConfig.getApiServer() + this.getContext(), callback);
  }

  public getLatestSurveyVersion(surveyId: string, callback?: Function) {
    return this.httpGet(this.configService.environmentConfig.getApiServer() + this.getContext() + "/" + this.translateService.currentLang + '/' + surveyId + '/Latest', callback);
  }

}
