import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { BaseRestService } from './rest.service';

@Injectable({
  providedIn: 'root'
})
export class UserQuestionRestService extends BaseRestService {

  URL_POC_TAG = 'userquestion';

  getContext(): string {
    return this.URL_POC_TAG;
  }

  constructor(public configService: ConfigService, public http: HttpClient) {
    super(configService, http);
  }

  public getAllUserQuestionFromUserSurvey(userSurveyId: string, callback?: Function) {
    return this.httpGet(this.configService.environmentConfig.getApiServer() + this.getContext() + "/all/" + userSurveyId, callback);
  }

  public getAllFromQuestionVersion(questionVersionId: string, callback?: Function) {
    return this.httpGet(this.configService.environmentConfig.getApiServer() + this.getContext() + "/all/questionVersion/" + questionVersionId, callback);
  }

  public getById(userQuestionId: string, callback?: Function) {
    return this.httpGet(this.configService.environmentConfig.getApiServer() + this.getContext() + "/" + userQuestionId, callback);
  }
}