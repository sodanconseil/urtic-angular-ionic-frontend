import { TestBed } from '@angular/core/testing';

import { UserAnswerRestService } from './user-answer-rest.service';

describe('UserAnswerRestService', () => {
  let service: UserAnswerRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserAnswerRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
