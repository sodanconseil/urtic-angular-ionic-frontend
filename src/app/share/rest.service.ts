import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ConfigService } from './config.service';
import { Observable, Observer, throwError } from 'rxjs';
import { catchError, flatMap, map, mergeMap, observeOn, tap } from 'rxjs/operators';

@Injectable()
export abstract class BaseRestService {
  private CODE_401 = 401;
  private CODE_500 = 500;
  constructor(public configService: ConfigService,
    public http: HttpClient) { }

  abstract getContext(): string;

  public handleError(err: HttpErrorResponse) {
    switch (err.status) {
      case this.CODE_401:
        console.log('error 401');
        return throwError(err);
      case this.CODE_500:
        this.configService.validError.emit();
        return throwError(err);
      default:
        return throwError(err);
    }
  }
  // tslint:disable-next-line:ban-types
  public get(id: string, callback?: Function) {
    return this.httpGet(this.configService.environmentConfig.getApiServer() + this.getContext() + '/' + id, callback);
  }
  // tslint:disable-next-line:ban-types
  public put(id: string, model: any, callback?: Function) {
    return this.httpPut(this.configService.environmentConfig.getApiServer() + this.getContext() + '/' + id, model, callback);
  }
  public post(model: any, callback?: Function) {
    return this.httpPost(this.configService.environmentConfig.getApiServer() + this.getContext(), model, callback);
  }
  // tslint:disable-next-line:ban-types
  public delete(id: string, callback?: Function) {
    return this.httpDelete(this.configService.environmentConfig.getApiServer() + this.getContext() + '/' + id, callback);
  }
  // tslint:disable-next-line:ban-types
  public getAll(callback?: Function) {
    return this.httpGet(this.configService.environmentConfig.getApiServer() + this.getContext(), callback);
  }
  // tslint:disable-next-line:ban-types
  public getFile(id: number, callback?: Function) {
    return this.httpGet(this.configService.environmentConfig.getApiServer() + '/attachment/' + id, callback, 'blob');
  }
  // tslint:disable-next-line:ban-types
  public postFile(file: any, callback?: Function) {
    return this.httpPost(this.configService.environmentConfig.getApiServer() + this.getContext(), file, callback);
  }

  public createOptions(responseContentType?: any, etag?: string, observe?: any): any {
    const options = {
      headers: new HttpHeaders({
        'Cache-Control': 'no-cache',
        'Access-Control-Expose-Headers': 'ETag',
        'Authorization': 'Bearer ' + JSON.parse(window.localStorage.getItem("OAUTH_TOKENS")).id_token
      }),
      responseContentType,
      observe
    };

    if (etag) {
      options.headers = options.headers.set('If-Match', etag);
    }

    return options;
  }

  private setEtagIfExisting(data: any): void {
    const etag: string = data.headers.get('ETag');
    if (etag) {
      data.body.etag = etag.replace(/"/g, '');
    }
  }

  // tslint:disable-next-line:ban-types
  public httpGet(url: string, callback?: Function, responseType?: string) {
    return this.http.get(url, this.createOptions(responseType ? responseType : 'text', undefined, 'response')).pipe(
      map((data: any) => {
        const toReturn = data.body;
        this.setEtagIfExisting(data);

        if (callback) {
          callback(toReturn);
        }
        return toReturn;
      }),
      catchError(this.handleError)
    );
  }

  // tslint:disable-next-line:ban-types
  public httpPost(url: string, model: any, callback?: Function, responseType?: string) {
    return this.http.post(url, model, this.createOptions(responseType ? responseType : 'text', undefined, 'response')).pipe(
      map((data: any) => {
        const toReturn = data.body;
        this.setEtagIfExisting(data);

        if (callback) {
          callback(toReturn);
        }
        return toReturn;
      }),
      catchError(this.handleError)
    );
  }

  // tslint:disable-next-line:ban-types
  public httpPut(url: string, model: any, callback?: Function, responseType?: string) {
    const ifMatch = model.etag;
    delete model.etag;
    return this.http.put(url, model, this.createOptions(responseType ? responseType : 'text', ifMatch, 'response')).pipe(
      map((data: any) => {
        const toReturn = data.body;
        this.setEtagIfExisting(data);

        if (callback) {
          callback(toReturn);
        }
        return toReturn;
      }),
      catchError(this.handleError)
    );
  }

  // tslint:disable-next-line:ban-types
  public httpDelete(url: string, callback?: Function, responseType?: string) {
    return this.http.delete(url, this.createOptions(responseType ? responseType : 'json')).pipe(
      map((data: any) => {

        if (callback) {
          callback(data);
        }
        return data;
      }),
      catchError(this.handleError)
    );
  }
}