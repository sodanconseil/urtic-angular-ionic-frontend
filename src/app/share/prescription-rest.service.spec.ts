import { TestBed } from '@angular/core/testing';

import { PrescriptionRestService } from './prescription-rest.service';

describe('PrescriptionRestService', () => {
  let service: PrescriptionRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PrescriptionRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
