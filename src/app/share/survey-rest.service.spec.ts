import { TestBed } from '@angular/core/testing';

import { SurveyRestService } from './survey-rest.service';

describe('SurveyVersionRestService', () => {
  let service: SurveyRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SurveyRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
