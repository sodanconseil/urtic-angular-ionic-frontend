import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ConfigService } from './config.service';
import { BaseRestService } from './rest.service'

@Injectable({
  providedIn: 'root'
})
export class UserRestService extends BaseRestService {

  URL_POC_TAG = 'me';

  getContext(): string {
    return this.URL_POC_TAG;
  }

  constructor(public configService: ConfigService, public http: HttpClient) {
    super(configService, http);
  }

  public getKeycloakUser() {
    return this.httpGet(this.configService.environmentConfig.getApiServer() + this.getContext())
      .pipe(
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  public getMe() {
    return this.httpGet(this.configService.environmentConfig.getApiServer() + this.getContext());
  }

  public createMeUSer(model: any) {
    return this.httpPost(this.configService.environmentConfig.getApiServer() + this.getContext(), model);
  }

  public editUser(model: any, id: string) {
    return this.httpPut(this.configService.environmentConfig.getApiServer() + "users" + '/' + id, model);
  }

  public getAllMePrescription(callback?: Function) {
    return this.httpGet(this.configService.environmentConfig.getApiServer() + this.getContext() + '/prescriptions');
  }

  public getPrescriptionById(id: string, callback?: Function) {
    return this.httpGet(this.configService.environmentConfig.getApiServer() + this.getContext() + '/' + id + '/prescriptions');
  }


  public createMeEthnicity(ethnicityId: string) {
    return this.httpPut(this.configService.environmentConfig.getApiServer() + this.getContext() + '/ethnicities/' + ethnicityId, {});
  }
}
