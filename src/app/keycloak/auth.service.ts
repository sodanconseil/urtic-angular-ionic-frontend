/* eslint-disable eqeqeq */
/* eslint-disable eol-last */
/* eslint-disable @typescript-eslint/semi */
/* eslint-disable object-shorthand */
/* eslint-disable @typescript-eslint/quotes */
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
// eslint-disable-next-line @typescript-eslint/semi
import { Injectable } from '@angular/core'
import { JwtHelperService } from '@auth0/angular-jwt'
import { InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { StorageService } from './storage.Service';
import { v4 as uuidv4 } from 'uuid';
import keycloakConfig from '../../assets/keycloak.json';
import { Router } from '@angular/router';

@Injectable({ providedIn: "root" })
export class AuthService {

  // eslint-disable-next-line @typescript-eslint/naming-convention
  URI_base: 'http://40.86.254.187:8080/auth/';
  keycloakConfig: any;

  constructor(
    public http: HttpClient,
    public storage: StorageService,
    private jwtHelper: JwtHelperService,
    private inAppBrowser: InAppBrowser,
    private router: Router
  ) {
    this.keycloakConfig = {
      authServerUrl: keycloakConfig.config.authServerUrl,
      realm: 'Urtic', //realm-id
      clientId: 'UrticUI', // client-id
      redirectUri: 'localhost:8100'  //callback-url registered for client.
    };
  }

  public keycloakLogin(login: boolean): Promise<any> {
    return new Promise((resolve, reject) => {
      const url = this.createLoginUrl(this.keycloakConfig, login);

      const options: InAppBrowserOptions = {
        zoom: 'no',
        location: 'no',
        clearsessioncache: 'yes',
        clearcache: 'yes',
        toolbar: 'no'
      };
      const browser = this.inAppBrowser.create(url, '_blank', options);
      const listener = browser.on('loadstart').subscribe((event: any) => {
        const callback = encodeURI(event.url);
        //Check the redirect uri
        if (callback.indexOf(this.keycloakConfig.redirectUri) > -1) {
          listener.unsubscribe();
          browser.close();
          const code = this.parseUrlParamsToObject(event.url);
          this.getAccessToken(this.keycloakConfig, code).then(
            () => {
              const token = this.storage.OAUTH_TOKENS;
              resolve(token);
            },
            () => reject("Count not login in to keycloak")
          );
        }
      });

    });
  }

  parseUrlParamsToObject(url: any) {
    const hashes = url.slice(url.indexOf('?') + 1).split('&');
    return hashes.reduce((params, hash) => {
      const [key, val] = hash.split('=');
      return Object.assign(params, { [key]: decodeURIComponent(val) })
    }, {});
  }

  createLoginUrl(keycloakConfig: any, isLogin: boolean) {
    const state = uuidv4();
    const nonce = uuidv4();
    const responseMode = 'query';
    const responseType = 'code';
    const scope = 'openid';
    return this.getUrlForAction(keycloakConfig, isLogin) +
      '?client_id=' + encodeURIComponent(keycloakConfig.clientId) +
      //'&state=' + encodeURIComponent(state) +
      '&redirect_uri=' + encodeURIComponent(keycloakConfig.redirectUri) +
      '&response_mode=' + encodeURIComponent(responseMode) +
      '&response_type=' + encodeURIComponent(responseType) +
      '&scope=' + encodeURIComponent(scope); //+
    // '&nonce=' + encodeURIComponent(nonce);
  }

  createLogoutUrl(keycloakConfig: any, isLogin: boolean) {
    return this.getRealmUrl(keycloakConfig)
      + '/protocol/openid-connect/logout?'
      + 'id_token_hint=' + this.storage.OAUTH_TOKENS.id_token
      + '&post_logout_redirect_uri=' + encodeURIComponent('http://localhost:8100/')
  }

  getUrlForAction(keycloakConfig: any, isLogin: boolean) {
    return isLogin ? this.getRealmUrl(keycloakConfig) + '/protocol/openid-connect/auth'
      : this.getRealmUrl(keycloakConfig) + '/protocol/openid-connect/registrations';
  }

  loadUserInfo(): Promise<any> {
    const url = this.getRealmUrl(this.keycloakConfig) + '/protocol/openid-connect/userinfo';
    const headers = this.getAccessHeaders(this.storage.OAUTH_TOKENS, 'application/json');
    return this.http.get(url, { headers: headers }).toPromise();

  }


  testLogout() {
    const url = this.getRealmUrl(this.keycloakConfig) + '/protocol/openid-connect/logout'
    // const headers = this.getAccessHeaders(this.storage.OAUTH_TOKENS, 'application/json');
    // const headers = new HttpHeaders({
    //   'Authorization': 'Bearer ' + this.storage.OAUTH_TOKENS.access_token,
    //   'Access-Control-Allow-Origin': '*',
    //   "withcredentials": withcredentials
    // })

    const headers = new HttpHeaders()
      .set('Authorization', 'Bearer ' + this.storage.OAUTH_TOKENS.access_token)
      .set('Content-Type', 'application/x-www-form-urlencoded')
    const body = {
      "client_id": this.keycloakConfig.clientId,
      "refresh_token": this.storage.OAUTH_TOKENS.refresh_token
    };
    // return this.http.post(url, { header, body }).toPromise();
    return this.http.post(url, body, { headers }).toPromise();
  }

  getAccessToken(kc: any, authorizationResponse: any) {
    const URI = this.getTokenUrl();
    const body = this.getAccessTokenParams(authorizationResponse.code, kc.clientId, kc.redirectUri);
    const headers = this.getTokenRequestHeaders();

    return this.createPostRequest(URI, body, {
      header: headers,
    }).then((newTokens: any) => {
      newTokens.iat = (new Date().getTime() / 1000) - 10; // reduce 10 sec to for delay
      this.storage.OAUTH_TOKENS = newTokens;
      window.localStorage.setItem('OAUTH_TOKENS', JSON.stringify(this.storage.OAUTH_TOKENS))
    });
  }

  refresh() {
    let newTokens;
    const decoded = this.jwtHelper.decodeToken(this.storage.OAUTH_TOKENS)
    if (decoded.iat + this.storage.OAUTH_TOKENS < (new Date().getTime() / 1000)) {
      const URI = this.getTokenUrl();
      const headers = this.getTokenRequestHeaders();
      const body = this.getRefreshParams(this.storage.OAUTH_TOKENS, this.keycloakConfig.clientId);
      newTokens = this.createPostRequest(URI, body, {
        headers: headers
      })
    } else {
      newTokens = this.storage.OAUTH_TOKENS;
    }
    newTokens.iat = (new Date().getTime() / 1000) - 10;
    this.storage.OAUTH_TOKENS = newTokens;
    return this.storage.OAUTH_TOKENS;
  }

  createPostRequest(uri, body, headers) {
    return this.http.post(uri, body, headers).toPromise()
  }

  getAccessHeaders(accessToken, contentType) {
    return new HttpHeaders()
      .set('Authorization', 'Bearer ' + accessToken)
      .set('Content-Type', contentType);
  }

  getRefreshParams(refreshToken, clientId) {
    return new HttpParams()
      .set('grant_type', 'refresh_token')
      .set('refresh_token', refreshToken)
      .set('client_id', encodeURIComponent(clientId))
  }

  getAccessTokenParams(code, clientId, redirectUrl) {
    return new HttpParams()
      .set('grant_type', 'authorization_code')
      .set('code', code)
      .set('client_id', encodeURIComponent(clientId))
      .set('redirect_uri', redirectUrl);
  }

  getTokenUrl() {
    return this.getRealmUrl(this.keycloakConfig) + '/protocol/openid-connect/token';
  }

  getTokenRequestHeaders() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded');

    const clientSecret = (this.keycloakConfig.credentials || {}).secret;
    if (this.keycloakConfig.clientId && clientSecret) {
      headers.set('Authorization', 'Basic ' + btoa(this.keycloakConfig.clientId + ':' + clientSecret));
    }
    return headers;
  }

  getRealmUrl(kc: any) {
    if (kc && kc.authServerUrl) {
      if (kc.authServerUrl.charAt(kc.authServerUrl.length - 1) == '/') {
        return kc.authServerUrl + 'realms/' + encodeURIComponent(kc.realm);
      } else {
        return kc.authServerUrl + '/realms/' + encodeURIComponent(kc.realm);
      }
    } else {
      return undefined;
    }
  }


  //LOGOUT

  public keycloakLogout(logout: boolean): Promise<any> {
    return new Promise((resolve, reject) => {
      const url = this.createLogoutUrl(this.keycloakConfig, logout);

      const options: InAppBrowserOptions = {
        zoom: 'no',
        location: 'no',
        clearsessioncache: 'yes',
        clearcache: 'yes',
        toolbar: 'no',
        hidden: 'yes'
      };
      const browser = this.inAppBrowser.create(url, '_blank', options);
      const listener = browser.on('loadstart').subscribe((event: any) => {
        listener.unsubscribe();
        browser.close();
      });

    });
  }
}