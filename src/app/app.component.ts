import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
    window.localStorage.clear();

    //Translation
    this.translateService.addLangs(["fr"])
    this.translateService.addLangs(["en"])

    var browserLang = this.translateService.getBrowserLang();
    this.translateService.setDefaultLang(browserLang);
    //
  }
}
