import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MedInformationComponent } from './components/med-information/med-information.component';
import { MedListSearchComponent } from './components/med-list-search/med-list-search.component';
import { MedManagerComponent } from './components/med-manager/med-manager.component';
import { CompletProfileFormComponent } from './components/complete-profile-form/complete-profile-form.component';
import { MedConfirmationDeleteComponent } from './components/med-confirmation-delete/med-confirmation-delete.component';
import { SurveyComponent } from './components/survey/survey.component';
import { SurveyCalendarComponent } from './components/survey/calendar/survey-calendar.component';
import { ErrorPageComponent } from './components/error-page/error-page.component';
import { CalendarModule } from "ion2-calendar";
import { QuestionSingleChoiceComponent } from './components/survey/questionType/question-single-choice/question-single-choice.component';
import { QuestionMultipleChoicesComponent } from './components/survey/questionType/question-multiple-choices/question-multiple-choices.component';
import { QuestionCalculatedComponent } from './components/survey/questionType/question-calculated/question-calculated.component';
// import { SurveyGroupComponent } from './components/home/survey-group/survey-group.component';
import { SurveyAnswerManagerComponent } from './components/survey/edit survey/survey-answer-manager/survey-answer-manager.component';
import { EditSurveyAnswerComponent } from './components/survey/edit survey/edit-survey-answer/edit-survey-answer.component';
import { QuestionStringComponent } from './components/survey/questionType/question-string/question-string.component';
import { NavigationButtonComponent } from './components/navigation-button/navigation-button.component';
// import { HomepageListItemComponent } from './components/home/homepage-list-item/homepage-list-item.component';

//Translation
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ProfileComponent } from './components/profile/profile.component';
import { PictureComponent } from './components/picture/picture.component';
import { HomePage } from './components/home/home.page';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
//




@NgModule({
    declarations: [
        AppComponent,
        MedManagerComponent,
        MedInformationComponent,
        MedListSearchComponent,
        CompletProfileFormComponent,
        SurveyComponent,
        MedConfirmationDeleteComponent,
        SurveyCalendarComponent,
        ErrorPageComponent,
        QuestionSingleChoiceComponent,
        QuestionMultipleChoicesComponent,
        QuestionCalculatedComponent,
        QuestionStringComponent,
        // SurveyGroupComponent,
        // HomepageListItemComponent,
        SurveyAnswerManagerComponent,
        EditSurveyAnswerComponent,
        NavigationButtonComponent,
        ProfileComponent,
        PictureComponent,
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
        ReactiveFormsModule,
        FormsModule,
        CalendarModule
    ],
    providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
        JwtHelperService,
        InAppBrowser],
    bootstrap: [AppComponent]
})
export class AppModule { }
