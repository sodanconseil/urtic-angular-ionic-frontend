# Urtic-angular-ionic-frontend


## Prérequis

* [Vscode](https://code.visualstudio.com/download)
* [Node](https://nodejs.org/en/)

### Commandes à éxécuter lorsque Node est installé ([Source de l'information](https://ionicframework.com/docs/angular/your-first-app)):


```
npm install -g @ionic/cli native-run cordova-res
npm install -g @angular/cli
```

## Android

### Prérequis supplémentaires

* [Jdk de Java ](https://github.com/adoptium/temurin11-binaries/releases/download/jdk-11.0.12+7/OpenJDK11U-jdk_x64_windows_hotspot_11.0.12_7.msi) (Ne pas oublier de cocher l'option Set JAVA_HOME variable)
* [Android Studio](https://developer.android.com/studio#downloads)
* [Android Webview debugging](https://marketplace.visualstudio.com/items?itemName=mpotthoff.vscode-android-webview-debug) (pour pouvoir debugger dans vscode)

### Directives pour le premier lancement
- Ouvrir android studio:
    - Aller dans tools -> sdk manager -> sdk tools -> cocher Android Sdk Command-Line Tools -> Apply

    ![Image](https://drive.google.com/uc?export=view&id=1zZp0vo-ioBZJr1sfQcXE2CjRPysCbPbx)

* [Créer une machine virtuelle android](https://developer.android.com/studio/run/managing-avds) ou connecter votre téléphone via adb à votre ordinateur
* Fermer Android studio
* Ajouter les platform-tools dans votre path (pour windows ajouter **%USERPROFILE%\AppData\Local\Android\Sdk\platform-tools** et suivre ce [guide](https://stackoverflow.com/questions/44272416/how-to-add-a-folder-to-path-environment-variable-in-windows-10-with-screensho))
* Cloner le repo et l'ouvrir dans vscode
* Ouvrir un terminal et entrer les commandes suivantes
    ```
    ionic build
    ionic cap sync android
    ionic cap open android
    ```

* Vous pouvez maintenant peser le bouton "play" dans android studio (devrait s'être ouvert automatiquement) et l'application devrait se lancer

### Directives pour les prochains lancements (live-reload + debugger dans vscode)

* Pour partir le programme sur le téléphone/ emulateur, ouvrir le projet dans vscode, ouvrir un terminal et éxécuter
    ```
    ionic capacitor run android -l --external
    ```
* Après avoir modifié un fichier du projet il suffira de sauvegarder le fichier modifié et ionic devrait mettre automatiquement à jour la version qui roule sur votre téléphone/émulateur

* Pour pouvoir debug dans vscode cliquez sur le bouton "play" de l'onglet debug -> Webview ionic starter -> Ionic App

    ![Image](https://drive.google.com/uc?export=view&id=1rRLHqUCPEeIHgqNHIK57WQYMQ-8NRq6Q)



* Vous pouvez maintenant placer des breakpoints et utiliser la console de debuggage (Debug console) dans vscode

## Ios

TODO  compléter 
```
    ionic build
    ionic cap sync ios
    ionic cap open ios
```
